<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@section('title') Festival Hunter - Map, Blog and Music Festival Guide @show</title>

        @section('meta_keywords')
            <meta name="keywords"
                  content="europe festivals, europe music festivals, music festivals, europe festival info, australia to tomorrowland,festivals, europe, map, list, accommodation, camping, trail,"/>
        @show

        @section('meta_author')
            <meta name="author" content="Jon Doe"/>
        @show

        @section('meta_description')
            <meta name="description"
                  content="Festival Hunter is creating a map of music festivals around the globe and providing tips, travel information and blogs to help you get the most out of your music festival experience"/>
        @show

        {{--		<link href="{{ asset('/css/all.css') }}" rel="stylesheet">--}}
        <link href="{{asset('css/all.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="/css/footer.css">

        {{-- TODO: Incorporate into elixer workflow. --}}
        <link rel="stylesheet"
              href="{{asset('assets/site/css/half-slider.css')}}">
        <link rel="stylesheet"
              href="{{asset('assets/site/css/justifiedGallery.min.css')}}"/>
        <link rel="stylesheet"
              href="{{asset('assets/site/css/lightbox.min.css')}}"/>
        <link rel="stylesheet" href="/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="/components/magnific-popup/dist/magnific-popup.css">

        @yield('styles')

                <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,400italic,700&amp;subset=latin,latin-ext'
              rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link rel="shortcut icon" href="{{ asset('assets/site/ico/favicon.ico') }}">
    </head>
    <body>
        @include('partials.nav')

        @include('flash::message')
        <div class="container">
        </div>
        @yield('content')

        @include('partials.footer')

                <!-- Scripts -->
        {{--<script src="{{ asset('/js/all.js') }}"></script>--}}
        <script src="{{ asset('js/all.js') }}"></script>
        @if( isset($map['js']))
            {!! $map['js'] !!}
        @endif

        {{-- TODO: Incorporate into elixir workflow. --}}
        <script src="{{asset('assets/site/js/jquery.justifiedGallery.min.js')}}"></script>
        <script src="{{asset('assets/site/js/lightbox.min.js')}}"></script>
        <script src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
        <script src="/components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>

        <script>
            $('#flash-overlay-modal').modal();
            $('div.alert').not('.alert-danger').delay(3000).slideUp(300);
        </script>
        @yield('scripts')

    </body>
</html>
