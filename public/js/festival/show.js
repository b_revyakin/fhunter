(function () {
  $('#add-to-plan').on('click', function() {
    $('#form-add-to-plan').submit();
  });

  $('#remove-from-plan').on('click', function() {
    $('#form-remove-from-plan').submit();
  });

  $('#add-i-been-here').on('click', function() {
    $('#form-add-i-been-here').submit();
  });

  $('#remove-i-been-here').on('click', function() {
    $('#form-remove-i-been-here').submit();
  });

  $('#add-to-want-list').on('click', function() {
    $('#form-add-to-want-list').submit();
  });

  $('#remove-from-want-list').on('click', function() {
    $('#form-remove-from-want-list').submit();
  });

})();