@extends('app')

@section('styles')
    <link rel="stylesheet" href="/css/common.css">
@stop

@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 center-align user-mini-dash">
                    @include('includes.profile')
                </div>
                <div class="col-md-9">
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div> <!-- end page-header -->
    <div class="container-fluid map">
        <div class="container">
            <div class="row">
                <div id="map-filters" class="col-md-3">
                    <h2>Filters</h2>
                    {!! Form::open(array('method' => 'GET','url' => '/map', 'class' => 'form-control')) !!}
                    <p>Date</p>
                    {!! Form::text('date', $inputs['date'], array('id' => 'datepicker', 'placeholder'=>'Date', 'class'=>'width-200' )) !!}<i
                            class="glyphicon glyphicon-calendar"></i>

                    <p>Price</p>
                    {!! Form::text('price_from', $inputs['price_from'], array('placeholder'=>'From')) !!}
                    {!! Form::text('price_to', $inputs['price_to'], array('placeholder'=>'To')) !!}

                    <p>Festival Name</p>
                    {!! Form::text('festival_name', $inputs['festival_name'], ['placeholder' => 'Festival Name', 'class'=>'width-200']) !!}

                    <p>Band name</p>
                    {!! Form::text('headliners', $inputs['headliners'], array('placeholder'=>'Band Name', 'class'=>'width-200')) !!}

                    <p>Rating</p>
                    <i class='glyphicon glyphicon-star'></i><i class='glyphicon glyphicon-star'></i><i
                            class='glyphicon glyphicon-star'></i><i class='glyphicon glyphicon-star'></i><i
                            class='glyphicon glyphicon-star-empty'></i><br>

                    <p>{!! Form::submit() !!}</p>
                    {!! Form::close() !!}
                </div>
                <div id="map" class="col-md-9">
                    {!! $map['html'] !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $('#datepicker').datepicker();
    </script>
@stop
