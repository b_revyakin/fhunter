<?php

namespace App\Repositories\TopTip;

use App\Entities\TopTip;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class TopTipsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TopTipRepositoryEloquent extends BaseRepository implements TopTipRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TopTip::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}