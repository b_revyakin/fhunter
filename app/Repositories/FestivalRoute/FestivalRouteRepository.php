<?php

namespace App\Repositories\FestivalRoute;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FestivalRouteRepository
 * @package namespace App\Repositories;
 */
interface FestivalRouteRepository extends RepositoryInterface
{
    //
}
