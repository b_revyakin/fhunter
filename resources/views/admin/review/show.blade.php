@extends('admin.layouts.default')

@section('title') {{ 'Reviews' }} :: @parent @stop

@section('styles')
    <link rel="stylesheet" href="/css/common.css">
@stop

@section('main')
    <div class="page-header">
        <h3>
            Checking review
        </h3>
    </div>
    <div class="container">
        <div class="row">
            <form action="{{ route('admin.reviews.submit', $review->id) }}" method="POST">
                <div class="col-md-12 section">
                    <label class="col-md-2 label-normal"><b>Author:</b></label>

                    <div class="col-md-10 stars">
                        <p>{{ $review->user->name }} ({{ $review->user->email }})</p>
                    </div>
                </div>
                <div class="col-md-12 section">
                    <label class="col-md-2 label-normal"><b>Rate:</b></label>

                    <div class="col-md-10 stars">
                        @for($i = 0; $i < $review->stars; $i++)
                            <i class='glyphicon glyphicon-star'></i>
                        @endfor
                        @for($i = 0; $i < (5 - $review->stars); $i++)
                            <i class='glyphicon glyphicon-star-empty'></i>
                        @endfor
                    </div>
                </div>
                <div class="col-md-12 section">
                    <label class="col-md-2 label-normal">
                        <b>Text review:</b>
                    </label>

                    <div class="col-md-8">
                        <p>{!! nl2br($review->review) !!}</p>
                    </div>
                    <div class="col-md-2">
                        <div>
                            <input type="radio" name="submit_text" value="1"
                                   required {{ ($review->approved !== null && $review->approved == 1 ? 'checked' : '') }}>
                            Approve
                        </div>
                        <div>
                            <input type="radio" name="submit_text" value="0"
                                   required {{ ($review->approved !== null && $review->approved == 0 ? 'checked' : '') }}>
                            Reject
                        </div>
                    </div>
                </div>
                @if($review->tips->count())
                    <div class="col-md-12 section">
                        <label class="col-md-2 label-normal">
                            <b>Top Tips:</b>
                        </label>

                        <div class="col-md-10">
                            @foreach($review->tips as $tip)
                                <div class="col-md-10 section padding-left-0">
                                    <p>
                                        {!! nl2br($tip->tip) !!}
                                    </p>
                                </div>
                                <div class="col-md-2 section padding-left-0">
                                    <div>
                                        <input type="radio" name="submit_tip_{{ $tip->id }}" value="1"
                                               required {{ ($tip->approved !== null && $tip->approved == 1 ? 'checked' : '') }}>
                                        Approve
                                    </div>
                                    <div>
                                        <input type="radio" name="submit_tip_{{ $tip->id }}" value="0"
                                               required {{ ($tip->approved !== null && $tip->approved == 0 ? 'checked' : '') }}>
                                        Reject
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                @if($review->photos->count())
                    <div class="col-md-12 section">
                        <label class="col-md-2 label-normal">
                            <b>Photos:</b>
                        </label>

                        <div class="col-md-10">
                            @foreach($review->photos as $photo)
                                <div class="col-md-10 section padding-left-0">
                                    <a class="photo" href="/{{ $photo->path }}">
                                        <img src="/{{ $photo->path }}" style="max-height: 100px">
                                    </a>
                                </div>
                                <div class="col-md-2 section padding-left-0">
                                    <div>
                                        <input type="radio" name="submit_photo_{{ $photo->id }}" value="1"
                                               required {{ ($photo->approved !== null && $photo->approved == 1 ? 'checked' : '') }}>
                                        Approve
                                    </div>
                                    <div>
                                        <input type="radio" name="submit_photo_{{ $photo->id }}" value="0"
                                               required {{ ($photo->approved !== null && $photo->approved == 0 ? 'checked' : '') }}>
                                        Reject
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                @endif

                <div class="section">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
    @parent

    <script>
        $('.photo').magnificPopup({
            type: 'image'
        })
    </script>
@stop
