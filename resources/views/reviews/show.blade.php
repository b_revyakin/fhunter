@extends('app')

@section('styles')
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/components/slick-carousel/slick/slick-theme.css">
    <link rel="stylesheet" href="/components/slick-carousel/slick/slick.css">
@stop

@section('scripts')
    <script src="/js/festival/show.js"></script>
    <script src="/components/slick-carousel/slick/slick.min.js"></script>
    <script>
        $(function () {
            $('#carousel').slick();

            $('#carousel').find('div.slick-slide:not(.slick-cloned)').find('a').magnificPopup({
                gallery: {
                    enabled: true
                },
                type: 'image'
            })
        });
    </script>
@stop

@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 center-align user-mini-dash">
                    @include('includes.profile')
                </div>
                <div class="col-md-9">
                    @include('festivals.short', ['festival' => $review->festival, 'onlyInfo' => true])
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 photos">
                <div class="col-md-12" data-slick='{"slidesToShow": 4, "slidesToScroll": 4}' id="carousel">
                    @foreach($review->photos as $photo)
                        <div class="img-carousel">
                            <a href="/{{ $photo->path }}">
                                <img src="/{{ $photo->path }}" alt="">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12">
                <label class="col-md-2 label-normal"><b>Status:</b></label>

                <div class="col-md-10 stars">
                    <p>
                        @if($review->approved !== null)
                            {{ $review->approved ? 'Approved' : 'Rejected' }}
                        @else
                            Not checked
                        @endif
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <label class="col-md-2 label-normal"><b>Rate:</b></label>

                <div class="col-md-10 stars">
                    @for($i = 0; $i < $review->stars; $i++)
                        <i class='glyphicon glyphicon-star'></i>
                    @endfor
                    @for($i = 0; $i < (5 - $review->stars); $i++)
                        <i class='glyphicon glyphicon-star-empty'></i>
                    @endfor
                </div>
            </div>
            <div class="col-md-12">
                <label class="col-md-2 label-normal">
                    <b>Text review:</b>
                </label>

                <div class="col-md-10">
                    <p>{!! nl2br($review->review) !!}</p>
                </div>
            </div>
            @if($review->tips->count())
                <div class="col-md-12">
                    <label class="col-md-2 label-normal">
                        <b>Top Tips:</b>
                    </label>

                    <div class="col-md-10">
                        @foreach($review->tips as $tip)
                            <p>
                                {!! nl2br($tip->tip) !!}
                                @if($tip->approved !== null)
                                    ({{ $tip->approved ? 'Approved' : 'Rejected' }})
                                @else
                                    (Not checked)
                                @endif
                            </p>
                        @endforeach
                    </div>
                </div>
            @endif
            @if($review->approved === null)
                <a class="btn btn-primary col-md-offset-11 col-md-1" href="{{ route('reviews.edit', $review->id) }}">Edit</a>
            @endif
        </div>
    </div>
@stop
