<?php

namespace App\Console\Commands;

use App\Festival;
use App\Repositories\Festival\FestivalRepository;
use Buzz\Browser as Browser;
use Illuminate\Console\Command;
use MetalMatze\LastFm\LastFm as LastFm;

class FetchFestivals extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'FetchFestivals';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches Festivals from Last.FM API';
    /**
     * @var FestivalRepository
     */
    private $festival;

    /**
     * Create a new command instance.
     *
     * @param FestivalRepository $festival
     */
    public function __construct(FestivalRepository $festival)
    {
        parent::__construct();

        $this->festival = $festival;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $countries = array("Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Barbuda", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Trty.", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Caicos Islands", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Futuna Islands", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard", "Herzegovina", "Holy See", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Jan Mayen Islands", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Korea (Democratic)", "Kuwait", "Kyrgyzstan", "Lao", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "McDonald Islands", "Mexico", "Micronesia", "Miquelon", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "Nevis", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Principe", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre", "Saint Vincent", "Samoa", "San Marino", "Sao Tome", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia", "South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Grenadines", "Timor-Leste", "Tobago", "Togo", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Turks Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Minor Outlying Islands", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (US)", "Wallis", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");

        // frameworks, e.g. laravel4, will automatically inject (new Browser)
        $lastfm = new Lastfm(new Browser);
        $lastfm->setApiKey('62be19f9ce0c6fbd1d65d381e979724e');
        foreach ($countries as $country) {
            $venueFestivals = $lastfm->geo_getEvents(array(
                'location' => $country,
                'festivalsonly' => 1,
                'limit' => '999'
            ));
            //sleep(0.25);
            //ToDo: clear debugging

            echo '<h1>' . $country . '</h1>';
            var_dump(json_decode($venueFestivals));

            if (!property_exists(json_decode($venueFestivals), 'error')) {
                $festivals = json_decode($venueFestivals)->events->event;

                // var_dump($festivals);

                //ToDo: remove duplication code!
                if (is_object($festivals)) {
                    $festivals = [$festivals];
                }

                foreach ($festivals as $fest) {
                    var_dump($fest);
                    $festival = new Festival;
                    $festival->lastfm_fest_id = $fest->id;
                    $festival->start_date = $fest->startDate;
                    $festival->name = $fest->title;
                    $festival->city = $fest->venue->location->city;
                    $festival->country = $fest->venue->location->country;
                    $festival->street = $fest->venue->location->street;
                    $festival->postalcode = $fest->venue->location->postalcode;
                    $festival->geo_lat = $fest->venue->location->{'geo:point'}->{'geo:lat'};
                    $festival->geo_long = $fest->venue->location->{'geo:point'}->{'geo:long'};
                    $festival->website = $fest->website;
                    $festival->other_artists = json_encode($fest->artists->artist);
                    $festival->headliners = $fest->artists->headliner;
                    $festival->info = strip_tags($fest->description);
                    $festival->save();
                }
            }
        }
    }

}
