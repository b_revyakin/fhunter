<?php

namespace App\Repositories\UserPlan;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserPlanRepository
 * @package namespace App\Repositories;
 */
interface UserPlanRepository extends RepositoryInterface
{
    //
}
