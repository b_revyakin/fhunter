<?php

namespace App\Events\Review;

use App\Entities\Review;
use App\Entities\User;

class SubmitReviewEvent extends ReviewEvent
{
    /**
     * Create a new event instance.
     *
     * @param Review $review
     * @param User $user
     */
    public function __construct(Review $review, User $user)
    {
        parent::__construct($review, $user);

        $this->causeSlug = 'review.text';
        $this->causeDescription = 'Admin submitted user\'s text review';
    }
}
