<?php

namespace App\Events\Profile;

use App\Entities\User;

class AddPhoto extends ProfileEvent
{

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);

        $this->causeSlug = 'profile.photo';
        $this->causeDescription = 'User upload photo to profile';
    }
}
