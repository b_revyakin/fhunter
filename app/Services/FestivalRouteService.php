<?php

namespace App\Services;

use App\Repositories\Festival\FestivalRepository;
use App\Repositories\FestivalRoute\FestivalRouteRepository;
use Exception;

class FestivalRouteService extends BaseService
{
    private $festivalRoute;

    private $festival;

    function __construct(FestivalRouteRepository $festivalRoute, FestivalRepository $festival)
    {
        $this->festivalRoute = $festivalRoute;
        $this->festival = $festival;
    }


    public function create($attributes)
    {
        $this->beginTransaction();

        try {
            $festivalRoute = $this->festivalRoute->create($attributes);

            $this->attachFestivals($festivalRoute->id, $attributes['festival']);

            $this->commit();
        } catch (Exception $e) {
            $this->rollBackAndLogException($e);
        }

        return $festivalRoute;
    }

    protected function attachFestivals($festivalRouteId, array $festivalIds)
    {
        $festivalRoute = $this->festivalRoute->find($festivalRouteId);

        $position = 1;
        foreach ($festivalIds as $festivalId) {
            $festival = $this->festival->find($festivalId);

            $festivalRoute->attachFestival($festival, ['position' => $position++]);
        }
    }

    public function update($attributes, $id)
    {
        $this->beginTransaction();

        try {
            $this->detachAllFestivals($id);

            $this->attachFestivals($id, $attributes['festival']);

            $this->commit();
        } catch (Exception $e) {
            $this->rollBackAndLogException($e);
        }

    }

    protected function detachAllFestivals($id)
    {
        $festivalRoute = $this->festivalRoute->find($id);

        $festivalRoute->festivals()->detach();
    }

    public function delete($id)
    {
        $this->beginTransaction();
        try {
            $this->festivalRoute->delete($id);

            $this->commit();
        } catch (Exception $e) {
            $this->rollBackAndLogException($e);
        }
    }
}
