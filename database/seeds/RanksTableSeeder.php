<?php

use App\Repositories\Rank\RankRepository;
use Illuminate\Database\Seeder;

class RanksTableSeeder extends Seeder
{
    private $rank;

    private $ranks = [
        [
            'title' => 'Festival Rookie',
            'color' => 'Grey',
            'from'  => 0,
            'to'    => 20,
        ],
        [
            'title' => 'Festival Enthusiast',
            'color' => 'White',
            'from'  => 21,
            'to'    => 500,
        ],
        [
            'title' => 'Festival Head',
            'color' => 'Red',
            'from'  => 501,
            'to'    => 1000,
        ],
        [
            'title' => 'Festival Veteran',
            'color' => 'Blue',
            'from'  => 1001,
            'to'    => 2500,
        ],
        [
            'title' => 'Festival Gold',
            'color' => 'Grey',
            'from'  => 2500,
            'to'    => 4294967295,
        ],
    ];

    /**
     * RanksTableSeeder constructor.
     * @param RankRepository $rank
     */
    public function __construct(RankRepository $rank)
    {
        $this->rank = $rank;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->ranks as $rank) {
            $this->rank->create($rank);
        }
    }

}
