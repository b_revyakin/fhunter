<?php namespace App\Http\Controllers;

use App\Criteria\UserCriteria;
use App\Http\Requests;
use App\Http\Requests\ReviewRequest;
use App\Repositories\Festival\FestivalRepository;
use App\Repositories\Review\ReviewRepository;
use App\Services\ReviewService;
use Auth;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class ReviewsController extends Controller
{
    /**
     * @var FestivalRepository
     */
    private $festival;

    /**
     * @var ReviewService
     */
    private $review;

    /**
     * @var ReviewRepository
     */
    private $reviewRepository;

    /**
     * @param FestivalRepository $festival
     * @param ReviewService $review
     * @param ReviewRepository $reviewRepository
     */
    function __construct(FestivalRepository $festival, ReviewService $review, ReviewRepository $reviewRepository)
    {
        $this->festival = $festival;
        $this->review = $review;
        $this->reviewRepository = $reviewRepository;

        if(Auth::check()) {
            $this->reviewRepository->pushCriteria(new UserCriteria(Auth::user()));
        }
    }

    public function index()
    {
        $reviews = $this->reviewRepository->all();
        return view('reviews.index', compact('reviews'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Response
     */
    public function create($id)
    {
        $festivals = $this->festival->all()->sortBy('name');

        if ($id == 0) {
            $festival = $festivals->first();
        } else {
            $festival = $festivals->find($id);
        }

        $festivals = $festivals->lists('name', 'id');

        return view('reviews.create', compact('festival', 'festivals'));
    }

    public function store(ReviewRequest $request)
    {
        if (Auth::user()->reviews->contains('festival_id', $request->get('festival_id'))) {
            $request->session()->flash('error', 'You reviewed this festival before!');
            return redirect()->back();
        }

        try {
            $this->review->create(array_merge(
                $request->all(),
                ['user_id' => Auth::user()->id]
            ), $request->file('photos'));
        } catch (QueryException $e) {
            $request->session()->flash('error', 'We have some problem! Please contact with support!');
        } catch (Exception $e) {
            //ToDo: need handle
            dd($e);
        }

        return redirect()->route('reviews.index');
    }

    public function show($id)
    {
        try {
            $review = $this->reviewRepository->find($id);

            if (!Auth::user()->owns($review)) {
                abort(403, 'Permission denied');
            }
        } catch (ModelNotFoundException $e) {
            abort(403, 'Permission denied');
        }

        return view('reviews.show', compact('review'));
    }

    public function edit($id)
    {
        try {
            $review = $this->reviewRepository->find($id);

            if (!Auth::user()->owns($review) || $review->approved !== null) {
                abort(403, 'Permission denied');
            }

            $festivals = $this->festival->all()->sortBy('name')->lists('name', 'id');;
        } catch (ModelNotFoundException $e) {
            abort(403, 'Permission denied');
        }

        return view('reviews.edit', compact('festivals', 'review'));
    }

    public function update(ReviewRequest $request, $id)
    {
        try {
            $review = $this->reviewRepository->find($id);

            if (!Auth::user()->owns($review) || $review->approved !== null) {
                abort(403, 'Permission denied');
            }

            $this->review->update(array_merge(
                $request->all(),
                ['user_id' => Auth::user()->id]
            ), $id, $request->file('photos'));

            //ToDo: add flash message
        } catch (ModelNotFoundException $e) {
            abort(403, 'Permission denied');
        }

        return redirect()->route('reviews.show', $id);
    }

    public function destroy($id)
    {
        try {
            $review = $this->reviewRepository->find($id);

            if (!Auth::user()->owns($review) || $review->approved !== null) {
                abort(403, 'Permission denied');
            }

            $this->review->remove($id);

            //ToDo: add flash message!
        } catch (ModelNotFoundException $e) {
            abort(403, 'Permission denied');
        }

        return redirect()->route('reviews.index');
    }

}
