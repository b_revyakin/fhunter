<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserPointLog extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'user_points_log';

    protected $fillable = [
        'user_id',
        'cause_id',
        'cause_description',
        'festival_id',
        'review_id',
        'top_tip_id'
    ];

}
