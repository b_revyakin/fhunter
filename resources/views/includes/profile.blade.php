@if (Auth::guest())
    {!!  HTML::image('img/profile_pic.png') !!}
    <h4>Guest</h4>
    <div class="col-md-12">
        <a href="auth/login/facebook">
            <button type="button" class="btn btn-default fb">Sign in With Facebook</button>
        </a>
    </div>
    <div class="col-md-12">
        <a href="auth/login/google">
            <button type="button" class="btn btn-default google">Sign in With Google</button>
        </a>
    </div>
@else
    @if (empty(Auth::user()->avatar))
        {!!  HTML::image('img/profile_pic.png') !!}
    @else
        {!!  HTML::image(Auth::user()->avatar, Auth::user()->name, ['class' => 'avatar']) !!}
    @endif
    <h4>{{ Auth::user()->name }}</h4>
    <p>Points: {{ Auth::user()->points }} </p>
    <p>Festivals Attended: {{ Auth::user()->attenders->count() }} </p>
    <p>Festivals Reviewed: {{ Auth::user()->planner->count()  }} </p>
    <p>Photos Submited: 0 </p>
    <button type="button" class="btn btn-default">Rank: {{ Auth::user()->rank() }}</button>
    <div class="row">
        <div class="col-md-12">
            <a href="{!! URL::to('profile') !!}"><b>Profile</b></a>
            /
            <a href="{!! URL::to('my-planner') !!}"><b>Planner</b></a>
        </div>
    </div>


@endif
