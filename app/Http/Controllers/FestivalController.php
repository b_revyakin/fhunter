<?php

namespace App\Http\Controllers;

use App\Festival;
use App\Http\Requests;
use App\Repositories\Festival\FestivalRepository;
use App\Repositories\User\UserRepository;
use Auth;

class FestivalController extends Controller
{
    private $user;
    private $festival;

    /**
     * FestivalController constructor.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user, FestivalRepository $festival)
    {
        $this->user = $user;
        $this->festival = $festival;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function index($id = null)
    {
        $activeFestival = null;
        if ($id) {
            $activeFestival = $this->festival->find($id);
        }
        $festivals = Festival::all();

        return view('festivals.index')
            ->with(compact('festivals', 'activeFestival'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $festival = Festival::findOrFail($id);
        $festivalPlan = null;

        if (Auth::check()) {
            $festivalPlan = [
                'plan' => Auth::user()->planner()->where('festival_id', $festival->id)->first(),
                'attender' => Auth::user()->attenders()->where('festival_id', $festival->id)->first(),
                'want' => Auth::user()->wants()->where('festival_id', $festival->id)->first()
            ];
        }

        return view('festivals.show')
            ->with('festival', $festival)
            ->with('plan', $festivalPlan);
    }
}
