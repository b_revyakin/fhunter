<?php namespace App\Http\Controllers\Admin;

use App\ArticleCategory;
use App\Entities\Article;
use App\Entities\User;
use App\Festival;
use App\Http\Controllers\AdminController;
use App\Photo;
use App\PhotoAlbum;
use App\Video;
use App\VideoAlbum;

class DashboardController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $title = "Dashboard";

        $news = Article::count();
        $festivals = Festival::count();
        $newscategory = ArticleCategory::count();
        $users = User::count();
        $photo = Photo::count();
        $photoalbum = PhotoAlbum::count();
        $video = Video::count();
        $videoalbum = VideoAlbum::count();
        return view('admin.dashboard.index', compact('title', 'news', 'festivals', 'newscategory', 'video', 'videoalbum', 'photo',
            'photoalbum', 'users'));
    }
}