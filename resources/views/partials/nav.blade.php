<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/">{!!  HTML::image('img/logo.png') !!}</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ (Request::is('map') ? 'active' : '') }}">
                    <a href="{{ URL::route('map') }}">Map</a>
                </li>
                <li class="{{ (Request::routeLike(Request::fullUrl(), 'news') ? 'active' : '') }}">
                    <a href="{!! URL::route('news') !!}">Blog</a>
                </li>
                <li class="{{ (Request::is('festival') ? 'active' : '') }}">
                    <a href="{!! URL::route('festivals') !!}">Festival Map</a>
                </li>
                <li class="{{ (Request::is('festival-routers') ? 'active' : '') }}">
                    <a href="{!! URL::to('') !!}">Routes</a>
                </li>
                <li class="{{ (Request::is('write-review') ? 'active' : '') }}">
                    <a href="{!! URL::route('reviews.create', ['id' => 0]) !!}">Review/Submit Photos</a>
                </li>
                <li class="{{ (Request::is('festivals-ive-been') ? 'active' : '') }}">
                    <a href="{!! URL::route('reviews.index') !!}">Festivals I've Been</a>
                </li>
                <li class="{{ (Request::is('my-planner') ? 'active' : '') }}">
                    <a href="{!! URL::route('my-planner') !!}">Planners</a>
                </li>
                @if (Auth::guest())
                    <li class="{{ (Request::is('auth/login') ? 'active' : '') }}"><a
                                href="{!! URL::to('auth/login') !!}"><i
                                    class="fa fa-sign-in"></i> Sign In</a></li>
                    <li class="{{ (Request::is('auth/register') ? 'active' : '') }}"><a
                                href="{!! URL::to('auth/register') !!}">Sign Up</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->name }} <i
                                    class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::check() && Auth::user()->admin==1)
                                <li>
                                    <a href="{!! URL::to('admin/dashboard') !!}"><i class="fa fa-tachometer"></i>
                                        Dashboard</a>
                                </li>
                                <li role="presentation" class="divider"></li>
                            @endif
                            <li>
                                <a href="{!! URL::to('auth/logout') !!}"><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>

            <ul class="nav navbar-nav navbar-right">

            </ul>
        </div>
    </div>

</nav>
