<?php

Route::controller('company', 'StaticPagesController', [
    'getTerms' => 'static.terms',
    'getPrivacy' => 'static.privacy',
    'getContacts' => 'static.contacts',
    'getWeAre' => 'static.we-are',
    'getAbout' => 'static.about',
]);

//ToDo: named routes!

Route::get('festival', ['as' => 'festivals', 'uses' => 'FestivalController@index']);
Route::get('festival/{id}/preview', ['as' => 'festival.preview', 'uses' => 'FestivalController@index']);
Route::get('festival/{id}', ['as' => 'festival', 'uses' => 'FestivalController@show']);

Route::group(['middleware' => 'auth'], function () {

    Route::patch('profile/update', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);

});
Route::group(['prefix' => 'festival_routes', 'as' => 'festival_routes.'], function () {
    Route::get('', ['as' => 'index', 'uses' => 'FestivalRoutesController@getListFestivalRoutes']);
    Route::get('{id}', ['as' => 'show', 'uses' => 'FestivalRoutesController@getShowFestivalRoute']);
    Route::post('{id}/add-to-plan', ['as' => 'add-to-plan', 'uses' => 'FestivalRoutesController@postAddFestivalsToPlan']);
});

Route::get('/', ['as' => 'default', 'uses' => 'HomeController@index']);
Route::get('home', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('map', ['as' => 'map', 'uses' => 'PagesController@map']);

Route::get('video/{id}', ['as' => 'video', 'uses' => 'VideoController@show']);
Route::get('photo/{id}', ['as' => 'photo', 'uses' => 'PhotoController@show']);

//ToDo: do wuth provider!
// Route::get('auth/login/{provider?}', 'Auth\AuthController@login');
// Route::get('auth/login/{provider?}/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('auth/login/facebook', 'Auth\AuthController@redirectToProviderFB');
Route::get('auth/login/facebook/callback', 'Auth\AuthController@handleProviderFBCallback');
Route::get('auth/login/google', 'Auth\AuthController@redirectToProviderGoogle');
Route::get('auth/login/google/callback', 'Auth\AuthController@handleProviderGoogleCallback');


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
