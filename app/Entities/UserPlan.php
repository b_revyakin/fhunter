<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserPlan extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'user_festivals';

    protected $fillable = ['user_id', 'festival_id', 'type'];

    public function festival()
    {
        return $this->hasOne(\App\Entities\Festival::class, 'id', 'festival_id');
    }

}
