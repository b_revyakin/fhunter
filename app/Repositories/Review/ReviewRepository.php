<?php

namespace App\Repositories\Review;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReviewRepository
 * @package namespace App\Repositories;
 */
interface ReviewRepository extends RepositoryInterface
{
    //
}
