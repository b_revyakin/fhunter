<?php

namespace App\Events\Share;

use App\Entities\User;

class FollowUsOnInstagram extends LikeEvent
{
    /**
     * LikeEvent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);

        $this->causeDescription = 'Foolow Us on Instagram';
    }

}
