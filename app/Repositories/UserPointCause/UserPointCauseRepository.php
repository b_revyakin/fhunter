<?php

namespace App\Repositories\UserPointCause;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserPointCauseRepository
 * @package namespace App\Repositories;
 */
interface UserPointCauseRepository extends RepositoryInterface
{
    public function findBySlug($slug);
}
