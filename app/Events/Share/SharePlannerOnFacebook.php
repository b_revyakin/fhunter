<?php

namespace App\Events\Share;

use App\Entities\User;
use App\Events\Event;

class SharePlannerOnFacebook extends Event
{
    /**
     * Event constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);

        $this->causeSlug = 'social_network.share';

        $this->causeDescription = 'Shared planners on Facebook';
    }

}
