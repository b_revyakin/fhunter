@extends('app')

@section('title') 'Blog' :: @parent @stop

@section('content')
    <div class="container">
        <div class="row">
            @foreach($news as $new)
                <h3><a href="{{ URL::route('news.view', ['id' => $new->id]) }}">{{ $new->title }}</a></h3>
                <p>{!! $new->introduction() !!}</p>
                <div>
                    <span class="badge badge-info">Posted {{ $new->created_at }}</span>
                </div>
            @endforeach
        </div>
    </div>
@stop
