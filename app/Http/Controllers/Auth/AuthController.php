<?php namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Redirect;
use Socialite;

class AuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }


    /**
     * Social network login/register
     *
     */

    public function redirectToProviderGoogle(Guard $auth)
    {
        if (\Input::has('code')) {
        }
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderGoogleCallback(Guard $auth)
    {
        $user = Socialite::driver('google')->user();
        // OAuth Two Providers
        $token = $user->token;
        $authUser = $this->findOrCreateUser($user);
        Auth::login($authUser, true);
        return Redirect::to('/');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $User
     * @return User
     */
    private function findOrCreateUser($userData)
    {
        $user = User::where('provider_id', '=', $userData->id)->first();
        if (!$user) {
            $user = User::create([
                'name' => $userData->name,
                'username' => $userData->id,
                'email' => $userData->email,
                'provider' => '',
                'provider_id' => $userData->id,
                'avatar' => rtrim($userData->avatar, '?sz=50'),
                'confirmed' => '1'
            ]);
        }

        $this->checkIfUserNeedsUpdating($userData, $user);
        return $user;
    }

    private function checkIfUserNeedsUpdating($userData, $user)
    {

        $socialData = [
            'avatar' => $userData->avatar,
            'email' => $userData->email,
            'name' => $userData->name,
            'username' => $userData->nickname,
        ];
        $dbData = [
            'avatar' => $user->avatar,
            'email' => $user->email,
            'name' => $user->name,
            'username' => $user->username,
        ];

        if (!empty(array_diff($socialData, $dbData))) {
            $user->avatar = rtrim($userData->avatar, '?sz=50');
            $user->email = $userData->email;
            $user->name = $userData->name;
            $user->username = $userData->nickname;
            $user->save();
        }
    }

    public function redirectToProviderFB(Guard $auth)
    {
        if (\Input::has('code')) {
        }
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderFBCallback(Guard $auth)
    {
        $user = Socialite::driver('facebook')->user();
        // OAuth Two Providers
        $token = $user->token;
        $authUser = $this->findOrCreateUser($user);
        Auth::login($authUser, true);
        return Redirect::to('/');
    }

}
