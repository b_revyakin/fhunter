<?php

namespace App\Events\Festival;

use App\Entities\Festival;
use App\Entities\User;
use App\Events\Event;

class FestivalEvent extends Event
{
    /**
     * Event constructor.
     * @param User $user
     * @param Festival $festival
     */
    public function __construct(User $user, Festival $festival)
    {
        parent::__construct($user);

        $this->causeObject = $festival;
    }

}
