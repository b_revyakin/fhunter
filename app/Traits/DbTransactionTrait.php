<?php

namespace App\Traits;

use DB;
use Exception;
use Log;

trait DbTransactionTrait
{
    protected function beginTransaction()
    {
        DB::beginTransaction();
    }

    protected function commit()
    {
        DB::commit();
    }

    /**
     * @param Exception $exception
     * @throws Exception
     */
    protected function rollBackAndLogException(Exception $exception)
    {
        $this->rollBack();

        Log::debug($exception);
        throw $exception;
    }

    protected function rollBack()
    {
        DB::rollBack();
    }
}
