<?php

use App\Repositories\UserPointCause\UserPointCauseRepository;
use Illuminate\Database\Seeder;

class UserPointCausesTableSeeder extends Seeder
{
    private $cause;

    private $causes = [
        [
            'cause' => 'Likes or share in social networks',
            'slug' => 'social_network.follow',
            'points' => 5
        ],
        [
            'cause' => 'Update profile',
            'slug' => 'profile.photo',
            'points' => 5
        ],
        [
            'cause' => 'Add festival to plan',
            'slug' => 'planner.plan',
            'points' => 1
        ],
        [
            'cause' => 'Add to been list',
            'slug' => 'planner.been',
            'points' => 0.5
        ],
        [
            'cause' => 'Add to want list',
            'slug' => 'planner.want',
            'points' => 0.5
        ],
        [
            'cause' => 'Share planner on social network',
            'slug' => 'social_network.share',
            'points' => 10
        ],
        [
            'cause' => 'Rate starts',
            'slug' => 'review.rate',
            'points' => 5
        ],
        [
            'cause' => 'Submit text review',
            'slug' => 'review.text',
            'points' => 50
        ],
        [
            'cause' => 'Submit review\'s photo',
            'slug' => 'review.photo',
            'points' => 10
        ],
        [
            'cause' => 'Submit top tip',
            'slug' => 'review.top_tip',
            'points' => 1
        ],
    ];

    /**
     * UserPointCausesTableSeeder constructor.
     * @param UserPointCauseRepository $cause
     */
    public function __construct(UserPointCauseRepository $cause)
    {
        $this->cause = $cause;
    }

    public function run()
    {
        foreach ($this->causes as $cause) {
            $this->cause->create($cause);
        }
    }
}
