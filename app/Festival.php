<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Festival extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'location', 'geo_lat', 'geo_long', 'headliners', 'start_date', 'end_date', 'size', 'cost', 'info', 'why_to_go', 'website', 'other_artists'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function review()
    {
        return $this->hasMany('App\Review');
    }

    public function planned()
    {
        //ToDo: fix it!
        //return $this->hasMany('App\Planner', 'planned_festivals');
    }

    public function attended()
    {
        //ToDo: fix it!
        //return $this->hasMany('App\Planner', 'attended_festivals');
    }

    public function showArtists()
    {
        $artists = json_decode($this->other_artists);

        if (is_array(json_decode($this->other_artists))) {
            $artists = implode(', ', json_decode($this->other_artists));
        }

        return $artists;
    }

    public function scopeName($query, $name)
    {
        if ($name) {
            $query = $query->where('name', 'like', '%' . $name . '%');
        }

        return $query;
    }

    public function scopeHeadliners($query, $headliners)
    {
        if ($headliners) {
            $query = $query->where('headliners', 'like', '%' . $headliners . '%');
        }

        return $query;
    }

    public function scopeDate($query, $date)
    {
        if ($date) {
            //ToDo: need fix it!
            $query = $query->where('start_date', $date);
        }

        return $query;
    }

    public function scopePrice($query, $from, $to)
    {
        if ($from) {
            $query = $query->where('cost', '>=', $from);
        }

        if ($to) {
            $query = $query->where('cost', '<=', $to);
        }

        return $query;
    }


}
