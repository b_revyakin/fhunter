<?php

namespace App\Repositories\TopTip;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TopTipsRepository
 * @package namespace App\Repositories;
 */
interface TopTipRepository extends RepositoryInterface
{
    //
}
