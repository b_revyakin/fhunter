<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\Review\SubmitReviewEvent::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Review\SubmitPhotoEvent::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Review\SubmitTopTipEvent::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Review\RateStars::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Share\LikeOurFacebook::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Share\FollowUsOnTwitter::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Share\FollowUsOnInstagram::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Share\SharePlannerOnFacebook::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Profile\AddPhoto::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Festival\AddToPlan::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Festival\AddToBeenHere::class => [
            \App\Listeners\PointersLog::class,
        ],

        \App\Events\Festival\AddToWantList::class => [
            \App\Listeners\PointersLog::class,
        ],

        //It was from previous developer
        /*UserWasDeleted::class => [
            DeleteUserGeneratedContent::class,
        ],*/
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //It was from previous developer
        /*User::deleting(function ($user) {
            \Event::fire(new UserWasDeleted($user->id));
        });*/
    }

}
