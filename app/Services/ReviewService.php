<?php

namespace App\Services;

use App\Events\Review\SubmitPhotoEvent;
use App\Events\Review\SubmitReviewEvent;
use App\Events\Review\SubmitTopTipEvent;
use App\Repositories\Festival\FestivalRepository;
use App\Repositories\Review\ReviewRepository;
use App\Repositories\ReviewPhoto\ReviewPhotoRepository;
use App\Repositories\TopTip\TopTipRepository;
use Auth;
use Exception;

class ReviewService extends BaseService
{
    /**
     * @var FestivalRepository
     */
    private $festival;

    /**
     * @var ReviewRepository
     */
    private $review;

    /**
     * @var ReviewPhotoRepository
     */
    private $reviewPhoto;

    /**
     * @var TopTipRepository
     */
    private $topTip;

    /**
     * @param FestivalRepository $festival
     * @param ReviewRepository $review
     * @param ReviewPhotoRepository $reviewPhoto
     * @param TopTipRepository $topTip
     */
    function __construct(FestivalRepository $festival, ReviewRepository $review, ReviewPhotoRepository $reviewPhoto,
                         TopTipRepository $topTip)
    {
        $this->festival = $festival;
        $this->review = $review;
        $this->reviewPhoto = $reviewPhoto;
        $this->topTip = $topTip;
    }

    public function create($attributes, $photos)
    {
        $this->beginTransaction();

        try {
            $review = $this->review->create($attributes);

            $this->createTopTips($attributes['toptips'], $review->id);
            $this->uploadPhotos($photos, $review->id, $attributes['festival_id'], $attributes['user_id']);

            $this->commit();

            return $review;
        } catch (Exception $e) {
            $this->rollBackAndLogException($e);
        }
    }

    public function createTopTips(array $tips, $reviewId)
    {
        foreach ($tips as $tip) {
            if ($tip) {
                $this->topTip->create([
                    'review_id' => $reviewId,
                    'tip' => $tip
                ]);
            }
        }
    }

    public function uploadPhotos($photos, $reviewId, $festivalId, $userId)
    {
        $maxSize = 5 * 1024 * 1024;
        foreach ($photos as $photo) {
            if ($photo && $photo->isValid() && $photo->getSize() < $maxSize) {
                //Download photos to /photos/festivals/{id}/users/{id}
                $path = 'photos/festivals/' . $festivalId . '/users/' . $userId . '/';
                $filename = $photo->getClientOriginalName();
                $photo->move($path, $filename);
                $this->reviewPhoto->create([
                    'review_id' => $reviewId,
                    'path' => $path . $filename
                ]);
            }
        }
    }

    public function update(array $attributes, $id, $photos)
    {
        $this->beginTransaction();

        try {
            $review = $this->review->update($attributes, $id);

            $review->tips()->delete();
            $this->createTopTips($attributes['toptips'], $id);
            $this->uploadPhotos($photos, $id, $attributes['festival_id'], $attributes['user_id']);

            $this->commit();

            return $review;
        } catch (Exception $e) {
            $this->rollBackAndLogException($e);
        }
    }

    public function remove($id)
    {
        $this->review->delete($id);
    }

    /**
     * All events will fire once when in first admin submit review
     *
     * @param array $attributes
     * @param $reviewId
     * @throws Exception
     */
    public function submitReview(array $attributes, $reviewId)
    {
        $this->beginTransaction();

        try {
            $user = Auth::user();
            $review = $this->review->find($reviewId);
            $first = ($review->approved === null);

            $review->approved = $attributes['submit_text'];
            $review->save();
            if ($first && $review->approved) {
                event(new SubmitReviewEvent($review, $user));
            }

            $tips = [];
            $photos = [];
            foreach (array_keys($attributes) as $key) {
                if (preg_match('/submit_tip_*/', $key)) {
                    $tips[] = substr($key, 11);
                } elseif (preg_match('/submit_photo_*/', $key)) {
                    $photos[] = substr($key, 13);
                }
            }

            foreach ($tips as $tipId) {
                $tip = $this->topTip->find($tipId);
                $tip->approved = $attributes['submit_tip_' . $tipId];
                $tip->save();

                if ($first && $tip->approved) {
                    event(new SubmitTopTipEvent($review, $user));
                }
            }

            foreach ($photos as $photoId) {
                $photo = $this->reviewPhoto->find($photoId);
                $photo->approved = $attributes['submit_photo_' . $photoId];
                $photo->save();
                if ($first && $photo->approved) {
                    event(new SubmitPhotoEvent($review, $user));
                }
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollBackAndLogException($e);
        }
    }

}
