<?php namespace App\Http\Controllers;

use App\Entities\Festival;
use App\Http\Requests;
use App\Repositories\Festival\FestivalRepository;
use App\Repositories\Rank\RankRepository;
use App\Repositories\UserPlan\UserPlanRepository;
use Auth;

class PlannerController extends Controller
{
    private $festival;

    private $userPlan;

    private $rank;

    /**
     * PlannerController constructor.
     * @param FestivalRepository $festival
     * @param UserPlanRepository $userPlan
     * @param RankRepository $rank
     */
    public function __construct(FestivalRepository $festival, UserPlanRepository $userPlan, RankRepository $rank)
    {
        $this->festival = $festival;
        $this->userPlan = $userPlan;
        $this->rank = $rank;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $planners = Auth::user()->planner;
        $attenders = Auth::user()->attenders;
        $wants = Auth::user()->wants;

        return view('pages.planner', compact('planners', 'attenders', 'wants'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show()
    {
        //ToDo: fix it!
        $attendedfestivals = Planner::where('user_id', Auth::user()->id)->where('planned_festivals', 0)->get();

        $attendedfestivals_array = array();

        foreach ($attendedfestivals as $attendedfestival) {
            $attendedfestivals_array[] = $attendedfestival->attended_festivals;
        }
        $attendedfestivals = Festival::find($attendedfestivals_array);

        return view('pages.festivals-ive-been', compact('attendedfestivals'));
    }

    public function postAddToWantList($festivalId)
    {
        Auth::user()->attachFestival($festivalId, Festival::WANT);

        return redirect()->back();
    }

    public function postDeleteFromWantList($festivalId)
    {
        Auth::user()->detachFestival($festivalId, Festival::WANT);

        return redirect()->back();
    }

    public function postAddToPlan($festivalId)
    {
        Auth::user()->attachFestival($festivalId, Festival::PLAN);

        return redirect()->back();
    }

    public function postRemoveFromPlan($festivalId)
    {
        Auth::user()->detachFestival($festivalId, Festival::PLAN);

        return redirect()->back();
    }

    public function postAddToBeenHere($festivalId)
    {
        Auth::user()->attachFestival($festivalId, Festival::HAVE_BEEN);

        return redirect()->back();
    }

    public function postRemoveFromBeenHere($festivalId)
    {
        Auth::user()->detachFestival($festivalId, Festival::HAVE_BEEN);

        return redirect()->back();
    }

}
