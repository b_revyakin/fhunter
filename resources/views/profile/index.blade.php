@extends('app')

@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h1>My profile</h1>
                </div>
                <div class="col-md-9">
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div> <!-- end page-header -->

    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>

            <div class="col-md-10">
                <h2>{{ $user->name }}</h2>

                {!! Form::model($user, ['url' => 'profile/update/', 'method' => 'PATCH']) !!}
                <div class="form-group">
                    <label for="name">Name</label>
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">e-mail</label>
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Update profile', ['class' => 'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}

            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>
@endsection
@stop
