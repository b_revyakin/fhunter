@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('main')

    <div class="page-header">
        <h3>
            {{$title}}
        </h3>
    </div>

    <div class="row">
        <form method="POST" action="{{ route('admin.festival_routes.store') }}" class="form-horizontal">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                </div>
            </div>

            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-5">
                    <textarea name="description" id="description" cols="30" rows="10"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Festivals:</label>
                <div class="col-sm-10" id="festival-container">

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-info" id="btn-add-festival">Add Festival</button>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Create Festival Route</button>
            </div>
        </form>
    </div>

    @include('admin.festival-routes.festival_input', ['festivals' => $festivals, 'selectedFestival' => null])
@stop

@section('scripts')
    @parent

    <script src="/js/festival-routes.js"></script>
@stop
