<?php

namespace App\Entities;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Festival extends \App\Festival implements Transformable
{
    use TransformableTrait;

    const
        PLAN = 1,
        HAVE_BEEN = 2,
        WANT = 3;

    public function routes()
    {
        return $this->belongsToMany(FestivalRoute::class, 'festival_route_pivot', 'festival_id', 'route_id');
    }
}
