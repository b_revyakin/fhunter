$(function () {
  $.fn.moveUp = function () {
    $.each(this, function () {
      $(this).after($(this).prev());
    });
  };
  $.fn.moveDown = function () {
    $.each(this, function () {
      $(this).before($(this).next());
    });
  };

  function updateButtonsAction() {
    updateRemoveButton();
    updateUpButton();
    updateDownButton();
  }

  function updateRemoveButton() {
    $('.btn-remove-festival').on('click', function () {
      $(this).parent().remove();
    });
  }

  function updateUpButton() {
    $('.btn-up-festival').on('click', function () {
      $(this).parent().moveUp();
    });
  }

  function updateDownButton() {
    $('.btn-down-festival').on('click', function () {
      $(this).parent().moveDown();
    });
  }

  $('#btn-add-festival').on('click', function () {
    $('#festival-select').clone().removeAttr('id').removeClass('hidden').appendTo('#festival-container');
    updateButtonsAction();
  });

  updateButtonsAction();
});