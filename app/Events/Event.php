<?php

namespace App\Events;

use App\Entities\Festival;
use App\Entities\Review;
use App\Entities\TopTip;
use App\Entities\User;
use App\Entities\UserPointCause;
use Illuminate\Queue\SerializesModels;

abstract class Event
{
    use SerializesModels;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var UserPointCause
     */
    protected $causeSlug;

    /**
     * @var string
     */
    protected $causeDescription;

    /**
     * @var Festival|Review|TopTip
     */
    protected $causeObject;

    protected $causeRepository;

    /**
     * Event constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function getCause()
    {
        return $this->causeSlug;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getCauseDescription()
    {
        return $this->causeDescription;
    }

    public function getCauseObject()
    {
        return $this->causeObject;
    }

}
