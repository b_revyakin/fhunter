<?php

namespace App\Repositories\Festival;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FestivalRepository
 * @package namespace App\Repositories;
 */
interface FestivalRepository extends RepositoryInterface
{
    //
}
