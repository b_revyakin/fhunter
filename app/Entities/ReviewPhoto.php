<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ReviewPhoto extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['review_id', 'path', 'approved'];

    public function scopeApproved($query)
    {
        return $query->where('approved', 1);
    }

}
