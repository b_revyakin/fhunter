<?php namespace App\Http\Controllers;

use App\Festival;
use App\Http\Requests;
use Gmaps;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PagesController extends Controller
{

    public function welcome()
    {
        return view('pages.welcome');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function map(Request $request)
    {
        // TODO rating
        // TODO zoom to all markers
        $inputs = Collection::make([
            'date' => $request->get('date'),
            'festival_name' => $request->get('festival_name'),
            'headliners' => $request->get('headliners'),
            'price_from' => $request->get('price_from'),
            'price_to' => $request->get('price_to')
        ]);

        $festivals = Festival::query()
            ->name($inputs->get('festival_name'))
            ->headliners($inputs->get('headliners'))
            ->date($inputs->get('date'))
            ->price($inputs->get('price_from'), $inputs->get('price_to'))
            ->select(array('id', 'name', 'geo_lat', 'geo_long', 'headliners', 'info'))->limit(999)->get();

        $map = $this->createGMaps($festivals);

        return view('pages.map')
            ->with('map', $map)
            ->with('inputs', $inputs->all());
    }

    /**
     * return GMaps
     * @param array $listFestival
     * @return array
     */
    protected function createGMaps($listFestival)
    {
        $config = array();
        $config['center'] = 'auto';
        $config['zoom'] = '7';
        $config['cluster'] = TRUE;
        $config['clusterMaxZoom'] = '12';

        Gmaps::initialize($config);

        foreach ($listFestival as $festival) {
            if (!empty($festival['geo_lat'] and $festival['geo_long'])) {
                $marker = [];
                $marker['position'] = $festival['geo_lat'] . ', ' . $festival['geo_long'];
                $marker['title'] = $festival['name'];
                $marker['infowindow_content'] = '<h5>' . $festival['name'] . '</h5><h6>Headliner:<b> ' . $festival['headliners'] . '</b></h6><p>' . $festival['info'] . '</p><p><a href="/festival/' . $festival['id'] . '">Click for more</a></p>';
                $marker['animation'] = 'DROP';
                Gmaps::add_marker($marker);
            }
        }

        return Gmaps::create_map();
    }
}
