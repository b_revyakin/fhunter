<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Article extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content'];

    /**
     * Returns a formatted post content entry,
     * this ensures that line breaks are returned.
     *
     * @return string
     */
    public function content()
    {
        return nl2br($this->content);
    }

    /**
     * Returns a formatted post content entry,
     * this ensures that line breaks are returned.
     *
     * @return string
     */
    public function introduction()
    {
        return nl2br($this->introduction);
    }

    /**
     * Get the post's author.
     *
     * @return User
     */
    public function author()
    {
        return $this->belongsTo(\App\Entities\User::class, 'user_id');
    }

    /**
     * Get the post's language.
     *
     * @return Language
     */
    public function language()
    {
        return $this->belongsTo(\App\Language::class);
    }

    /**
     * Get the post's category.
     *
     * @return ArticleCategory
     */
    public function category()
    {
        return $this->belongsTo(\App\ArticleCategory::class);
    }

}
