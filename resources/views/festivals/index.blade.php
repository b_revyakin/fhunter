@extends('app')

@section('styles')
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/festival-list.css">
@stop

@section('content')

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 center-align user-mini-dash">
                    @include('includes.profile')
                </div>
                <div class="col-md-9">
                    @if($activeFestival)
                        @include('festivals.short', ['festival' => $activeFestival, 'onlyInfo' => false])
                    @endif
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div> <!-- end page-header -->
    <div class="container-fluid map">
        <div class="container">
            @foreach($festivals->chunk(3) as $festivalSet)
                <div class="row">
                    @foreach($festivalSet as $festival)
                        <div class="col-md-4 festival-block {{ $activeFestival && $activeFestival->id == $festival->id ? 'festival-active' : null}}">
                            <a href="{{ route('festival.preview', $festival->id) }}"><span>{{ $festival->name }}</span></a>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $('.festival-block').on('click', function () {
            window.location = $(this).find('a').attr('href');
        });
    </script>
@stop
