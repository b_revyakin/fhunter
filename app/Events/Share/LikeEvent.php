<?php

namespace App\Events\Share;

use App\Entities\User;
use App\Events\Event;

class LikeEvent extends Event
{

    /**
     * LikeEvent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);

        $this->causeSlug = 'social_network.follow';
    }
}
