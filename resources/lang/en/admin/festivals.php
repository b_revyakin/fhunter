<?php

return [
    'festivals' => 'Festivals',
    'name' =>   'Name',
    'location' =>   'Location',
    'headliners' =>   'Headliners',
    'country' =>   'Country',
    'start_date' =>   'Start Date',
    'end_date' =>  'End Date',
    'yes' => 'Yes',
    'no' => 'No',
    'email' => 'Email',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'activate_festival' => 'Active'
];