<?php

namespace App\Repositories\UserPlan;

use App\Entities\UserPlan;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserPlanRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserPlanRepositoryEloquent extends BaseRepository implements UserPlanRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserPlan::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}