<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserPointsLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_points_log', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('cause_id')->unsigned();
            $table->foreign('cause_id')->references('id')->on('user_point_causes');
            $table->string('cause_description');

            //ToDo: see morphs type!
            $table->integer('festival_id')->unsigned()->nullable();
            $table->foreign('festival_id')->references('id')->on('festivals')->onDelete('cascade');

            $table->integer('review_id')->unsigned()->nullable();
            $table->foreign('review_id')->references('id')->on('reviews')->onDelete('cascade');

            $table->integer('top_tip_id')->unsigned()->nullable();
            $table->foreign('top_tip_id')->references('id')->on('top_tips')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_points_log');
    }
}
