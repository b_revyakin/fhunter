<?php

namespace App\Http\Controllers\Admin;

use App\Criteria\SortByNewestStartDate;
use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Repositories\Festival\FestivalRepository;
use App\Repositories\FestivalRoute\FestivalRouteRepository;
use App\Services\FestivalRouteService;
use Illuminate\Http\Request;

class FestivalRoutesController extends AdminController
{
    private $festivalRoute;

    private $festival;

    private $festivalRouteService;

    function __construct(FestivalRouteRepository $festivalRoute, FestivalRepository $festival, FestivalRouteService $festivalRouteService)
    {
        parent::__construct();

        $this->festivalRoute = $festivalRoute;
        $this->festival = $festival->pushCriteria(new SortByNewestStartDate());
        $this->festivalRouteService = $festivalRouteService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $title = 'Festival Routes';

        $festivalRoutes = $this->festivalRoute->all();

        return view('admin.festival-routes.list')
            ->with(compact('title', 'festivalRoutes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $title = 'New Festival Route';

        $festivals = $this->festival->all();

        return view('admin.festival-routes.new')
            ->with(compact('title', 'festivals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->festivalRouteService->create($request->all());

        return redirect()->route('admin.festival_routes.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $title = 'Edit Festival Route';

        $festivalRoute = $this->festivalRoute->find($id);

        $festivals = $this->festival->all();

        return view('admin.festival-routes.edit')
            ->with(compact('title', 'festivalRoute', 'festivals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->festivalRouteService->update($request->all(), $id);

        return redirect()->route('admin.festival_routes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->festivalRouteService->delete($id);

        return redirect()->route('admin.festival_routes.index');
    }
}
