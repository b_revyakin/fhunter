<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\FestivalRoute\FestivalRouteRepository;
use Gmaps;

class FestivalRoutesController extends Controller
{
    private $festivalRoute;

    /**
     * FestivalRoutesController constructor.
     * @param FestivalRouteRepository $festivalRoute
     */
    public function __construct(FestivalRouteRepository $festivalRoute)
    {
        $this->festivalRoute = $festivalRoute;
    }

    public function getListFestivalRoutes()
    {
        $festivalRoutes = $this->festivalRoute->all();

        return view('festival_routes.index', compact('festivalRoutes'));
    }

    public function getShowFestivalRoute($id)
    {
        $festivalRoute = $this->festivalRoute->find($id);

        $config = array();
        $config['center'] = 'auto';
        $config['zoom'] = '7';
        $config['cluster'] = TRUE;
        $config['clusterMaxZoom'] = '12';

        Gmaps::initialize($config);

        //ToDo: need add directions(routes)!
        foreach ($festivalRoute->festivals as $festival) {
            if (!empty($festival['geo_lat'] and $festival['geo_long'])) {
                $marker = [];
                $marker['position'] = $festival['geo_lat'] . ', ' . $festival['geo_long'];
                $marker['title'] = $festival['name'];
                $marker['infowindow_content'] = '<h5>' . $festival['name'] . '</h5><h6>Headliner:<b> ' . $festival['headliners'] . '</b></h6><p>' . $festival['info'] . '</p><p><a href="/festival/' . $festival['id'] . '">Click for more</a></p>';
                $marker['animation'] = 'DROP';
                Gmaps::add_marker($marker);
            }
        }

        $map = Gmaps::create_map();

        return view('festival_routes.show')
            ->with(compact('festivalRoute', 'map'));
    }

    public function postAddFestivalsToPlan($id)
    {

    }
}
