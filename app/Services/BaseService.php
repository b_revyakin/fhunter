<?php

namespace App\Services;

use App\Traits\DbTransactionTrait;

class BaseService
{
    use DbTransactionTrait;
}
