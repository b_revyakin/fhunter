@extends('app')

@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 center-align user-mini-dash">
                    @include('includes.profile')
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Festival</th>
                    <th>Stars</th>
                    <th>Text</th>
                    <th>Created</th>
                    <th>Submitted</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reviews as $review)
                    <tr>
                        <td><a href="{{ route('festival', $review->festival->id) }}">{{ $review->festival->name }}</a>
                        </td>
                        <td>{{ $review->stars }}</td>
                        <td>{!! nl2br(str_limit($review->review, 100)) !!}</td>
                        <td>{{ $review->created_at }}</td>
                        <td>
                            @if($review->approved !== null)
                                {{ $review->approved ? 'Approved' : 'Rejected' }}
                            @else
                                Not checked
                            @endif
                        </td>
                        <td>
                            <a class="btn btn-primary btn-xs" href="{{ route('reviews.show', $review->id) }}">Show</a>
                            @if($review->approved === null)
                                <a class="btn btn-info btn-xs" href="{{ route('reviews.edit', $review->id) }}">Edit</a>

                                <form action="{{ route('reviews.destroy', $review->id) }}" method="post"
                                      style="display: inline">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop
