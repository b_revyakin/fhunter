<?php

namespace App\Repositories\UserPointCause;

use App\Entities\UserPointCause;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserPointCauseRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserPointCauseRepositoryEloquent extends BaseRepository implements UserPointCauseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserPointCause::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param $slug
     * @return UserPointCause|null
     */
    public function findBySlug($slug)
    {
        return $this->findByField('slug', $slug)->first();
    }
}