@extends('app')
@section('title') Home :: @parent @stop
@section('content')
</div>
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 center-align user-mini-dash">
                @include('includes.profile')
            </div>
            <div class="col-md-9">
                <h1>Festival Hunter - Discover, Plan, Review, Win</h1>
            </div>
            <div class="col-md-6">
                <p>Welcome to Festival Hunter. We are building a community of festival heads who come to share their
                    knowledge, learn from others and hunt down the greatest Music Festival experiences.</p>

                <p>Create an account to record your Festival History, Submit photos and Revise and advance through the
                    rank you achieve the ultimate title of Festival Hunter.</p>
            </div>
            <div class="col-md-3">
                <p>Festival Hunter:</p>

                <p>A select group of people who have experienced the biggest and best the world of music festivals have
                    to other.</p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Festivals I've Been To</h1>
            @foreach ($attendedfestivals as $attendedfestival)
                <h2>{{ $attendedfestival->name }}</h2>
            @endforeach
        </div>
        <!-- end class="col-md-8" -->
    </div>
</div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#myCarousel').carousel({
            interval: 4000
        });
    </script>
@endsection
@stop
