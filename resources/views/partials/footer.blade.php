<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Do You Like Festivals?</h3>

                <p>Festival Hunter: A select group of people who have experienced the biggest and the best the world of
                    music festivals have to other.</p>
            </div>
            <div class="col-md-6">
                <span id="follow-us">Follow Us</span>
                <a href="https://www.facebook.com/festivalhunter"><img src="/img/button_facebook.png"
                                                                       alt="facebook_group" class="share-button"></a>
                <a href="https://instagram.com/festivalhunter/"><img src="/img/button_instagram.png"
                                                                     alt="instagram_group" class="share-button"></a>
                <a href="https://twitter.com/festivalhunter"><img src="/img/button_twitter.png" alt="twitter_group"
                                                                  class="share-button"></a>
            </div>
            <div class="col-md-6 links">
                <a href="{{ URL::route('static.terms') }}">Terms</a>
                <a href="{{ URL::route('static.privacy') }}">Privacy</a>
                <a href="{{ URL::route('static.contacts') }}">Contacts</a>
                <a href="{{ URL::route('static.we-are') }}">We Are</a>
            </div>
        </div>
    </div>
</div>
