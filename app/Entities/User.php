<?php

namespace App\Entities;

use App\Traits\HasFestival;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Model implements Transformable, AuthenticatableContract, CanResetPasswordContract
{
    use TransformableTrait, Authenticatable, CanResetPassword, HasFestival;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'confirmed',
        'confirmation_code',
        'avatar',
        'provider',
        'provider_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'confirmation_code'];


    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function planner()
    {
        return $this->festivals()->wherePivot('type', Festival::PLAN);
    }

    public function festivals()
    {
        return $this->belongsToMany(Festival::class, 'user_festivals');
    }

    public function attenders()
    {
        return $this->festivals()->wherePivot('type', Festival::HAVE_BEEN);
    }

    public function wants()
    {
        return $this->festivals()->wherePivot('type', Festival::WANT);
    }

    public function rank()
    {
        $points = $this->points;
        $rank = Rank::where('from', '<=', $points)->where('to', '>', $points)->first();
        return $rank->title;
    }

    public function owns($relation)
    {
        return $relation->user_id == $this->id;
    }

}
