<?php namespace App\Http\Controllers\Admin;

use App\Festival;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;


class FestivalController extends AdminController
{

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.festivals.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        return view('admin.festivals.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(UserRequest $request)
    {

        $festival = new Festival ();
        $festival->name = $request->name;
        $festival->city = $request->city;
        $festival->country = $request->country;
        $festival->street = $request->street;
        $festival->postalcode = $request->postalcode;
        $festival->geo_lat = $request->geo_lat;
        $festival->geo_long = $request->geo_long;
        $festival->headliners = $request->headliners;
        $festival->start_date = $request->start_date;
        $festival->end_date = $request->end_date;
        $festival->size = $request->size;
        $festival->cost = $request->cost;
        $festival->info = $request->info;
        $festival->why_to_go = $request->why_to_go;
        $festival->website = $request->website;
        $festival->other_artists = $request->other_artists;
        $festival->start_date = date($request->password);
        $festival->end_date = date($request->password);
        $festival->confirmed = $request->confirmed;
        $festival->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id)
    {

        $festival = Festival::find($id);
        return view('admin.festivals.create_edit', compact('festival'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(UserEditRequest $request, $id)
    {

        $festival = Festival::find($id);
        $festival->name = $request->name;
        $festival->confirmed = $request->confirmed;

        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $user->password = bcrypt($password);
            }
        }
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {
        $festival = Festival::find($id);
        // Show the page
        return view('admin.festivals.delete', compact('festival'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request, $id)
    {
        $festival = Festival::find($id);
        $festival->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $festivals = Festival::select(array('festivals.id', 'festivals.name', 'festivals.country', 'festivals.headliners', 'festivals.start_date', 'festivals.end_date'));

        return Datatables::of($festivals)
            // ->edit_column('confirmed')
            ->add_column('actions', '@if ($id!="1")<a href="{{{ URL::to(\'admin/festivals/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/festivals/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                @endif')
            ->remove_column('id')
            ->make();
    }

}
