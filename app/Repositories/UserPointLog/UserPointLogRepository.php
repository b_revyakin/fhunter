<?php

namespace App\Repositories\UserPointLog;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserPointLogRepository
 * @package namespace App\Repositories;
 */
interface UserPointLogRepository extends RepositoryInterface
{
    //
}
