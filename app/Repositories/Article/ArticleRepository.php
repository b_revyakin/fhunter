<?php

namespace App\Repositories\Article;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ArticleRepository
 * @package namespace App\Repositories;
 */
interface ArticleRepository extends RepositoryInterface
{
    //
}
