@section('styles')
    @parent

    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/festival-list.css">
@stop

@section('scripts')
    @parent

    <script src="/js/festival/short.js"></script>
@stop

<div class="row">
    <div class="pre-header">
        <div>
            @if(! $onlyInfo)
                <div class="col-md-6">
                    <div class="float-right">
                        @if(! (Auth::user()->planner()->where('festival_id', $festival->id)->first()))
                            <form method="post" action="{{ route('festival.add-to-plan', $festival->id) }}">
                                {{ csrf_field()  }}
                                <button class="btn btn-default">Add to plan</button>
                            </form>
                        @else
                            <form method="post" action="{{ route('festival.remove-from-plan', $festival->id) }}">
                                {{ csrf_field()  }}
                                <button class="btn btn-default">Remove from plan</button>
                            </form>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('reviews.create', $festival->id) }}" class="btn btn-default">Review</a>
                    <a href="{{ route('festivals') }}" class="btn btn-danger float-right">Close</a>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 festival-info">
        <div class="festival-short-info">
            <p><span class="label-normal indent-right"><b>{{ $festival->name }}</b></span></p>

            <p><span class="label-normal indent-right"><b>Headliners:</b></span>{{ $festival->headliners }}</p>

            <p><span class="label-normal indent-right"><b>Dates:</b></span>
                {{ $festival->start_date . ($festival->end_date ? ( ' - ' . $festival->end_date) : '') }}</p>

            @if($festival->cost)
                <p><span class="label-normal indent-right"><b>Cost:</b></span>{{ $festival->cost }}</p>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="festival-short-info">
            <p>{!! nl2br($festival->info) !!}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 text-align-center">
        Player
    </div>
    <div class="col-md-6 text-align-center">
        <a class="btn btn-primary" href="{{ route('festival', $festival->id) }}">View Full
            Info</a>
    </div>
</div>