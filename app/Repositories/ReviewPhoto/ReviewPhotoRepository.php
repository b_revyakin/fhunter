<?php

namespace App\Repositories\ReviewPhoto;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReviewPhotoRepository
 * @package namespace App\Repositories;
 */
interface ReviewPhotoRepository extends RepositoryInterface
{
    //
}
