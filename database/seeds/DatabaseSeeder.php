<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
    public function run()
    {
		Model::unguard();

        // Add calls to Seeders here
        $this->call('UsersTableSeeder');
		$this->call('LanguagesTableSeeder');
		$this->call('ArticleCategoriesTableSeeder');
		$this->call('ArticlesTableSeeder');
        $this->call('RanksTableSeeder');

		/*$this->call('PhotoAlbumsTableSeeder');
        $this->call('PhotosTableSeeder');
        $this->call('VideoAlbumsTableSeeder');
        $this->call('VideosTableSeeder');*/

        Model::reguard();

        $this->call(UserPointCausesTableSeeder::class);
    }

}
