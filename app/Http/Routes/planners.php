<?php

Route::group(['middleware' => 'auth'], function () {

    //Show planners festivals and where user have been
    Route::get('festivals-ive-been', ['as' => '', 'uses' => 'PlannerController@show']);
    Route::get('my-planner', ['as' => 'my-planner', 'uses' => 'PlannerController@index']);

    //Actions with festivals
    Route::post('festival/{id}/add-to-plan', ['as' => 'festival.add-to-plan',
        'uses' => 'PlannerController@postAddToPlan']);
    Route::post('festival/{id}/remove-from-plan', ['as' => 'festival.remove-from-plan',
        'uses' => 'PlannerController@postRemoveFromPlan']);
    Route::post('festival/{id}/add-been-here', ['as' => 'festival.add-been-here',
        'uses' => 'PlannerController@postAddToBeenHere']);
    Route::post('festival/{id}/remove-been-here', ['as' => 'festival.remove-been-here',
        'uses' => 'PlannerController@postRemoveFromBeenHere']);
    Route::post('festival/{id}/add-to-want-list', ['as' => 'festival.add-to-want-list',
        'uses' => 'PlannerController@postAddToWantList']);
    Route::post('festival/{id}/remove-from-want-list', ['as' => 'festival.remove-from-want-list',
        'uses' => 'PlannerController@postDeleteFromWantList']);

});
