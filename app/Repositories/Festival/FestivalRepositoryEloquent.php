<?php

namespace App\Repositories\Festival;

use App\Entities\Festival;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class FestivalRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FestivalRepositoryEloquent extends BaseRepository implements FestivalRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Festival::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}