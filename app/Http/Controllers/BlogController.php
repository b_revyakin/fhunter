<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\Article\ArticleRepository;

class BlogController extends Controller
{
    private $article;

    function __construct(ArticleRepository $article)
    {
        $this->article = $article;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //ToDo: add pagination or endless scroll
        $news = $this->article->all();

        return view('blog.news', compact('news'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $news = $this->article->find($id);

        return view('blog.view_news', compact('news'));
    }

}
