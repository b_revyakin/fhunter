<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{

    protected $table = "article_categories";

    /**
     * Returns a formatted post content entry,
     * this ensures that line breaks are returned.
     *
     * @return string
     */
    public function description()
    {
        return nl2br($this->description);
    }

    /**
     * Get the author.
     *
     * @return User
     */
    public function author()
    {
        return $this->belongsTo('App\Entities\User');
    }

    /**
     * Get the slider's images.
     *
     * @return array
     */
    public function articles()
    {
        return $this->hasMany(\App\Entities\Article::class);
    }

    /**
     * Get the category's language.
     *
     * @return Language
     */
    public function language()
    {
        return $this->belongsTo('App\Language');
    }
}
