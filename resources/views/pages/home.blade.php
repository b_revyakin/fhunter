@extends('app')

@section('title')
    Home :: @parent
@stop

@section('styles')
    @parent

    <link rel="stylesheet" href="/css/home.css">
@stop

@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 center-align user-mini-dash">
                    @include('includes.profile')
                </div>
                <div class="col-md-9">
                    <h1>Festival Hunter - Discover, Plan, Review, Win</h1>
                </div>
                <div class="col-md-6">
                    <p>Welcome to Festival Hunter. We are building a community of festival heads who come to share their
                        knowledge, learn from others and hunt down the greatest Music Festival experiences.</p>

                    <p>Create an account to record your Festival History, Submit photos and Revise and advance through
                        the rank you achieve the ultimate title of Festival Hunter.</p>
                </div>
                <div class="col-md-3">
                    <p>Festival Hunter:</p>

                    <p>A select group of people who have experienced the biggest and best the world of music festivals
                        have to other.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Location</h2>

                <div id="homemap">
                    <img src="/img/map_static.png" alt="festivals_map">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h2>Popular Routes</h2>
                        <a href="{!! URL::to('festival') !!}">{!!  HTML::image('img/roads.png') !!}</a>
                    </div>

                    <div class="col-md-6">
                        <h2>Submit Photos/Review</h2>
                        <a href="{!! URL::to('submit-photos') !!}">{!!  HTML::image('img/submit-photos.png') !!}</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h2>Festival I've Been To</h2>
                        <a href="{!! URL::to('festivals-ive-been') !!}">{!!  HTML::image('img/festivals.png') !!}</a>
                    </div>

                    <div class="col-md-6">
                        <h2>Planner</h2>
                        <a href="{!! URL::to('my-planner') !!}">{!!  HTML::image('img/my-planner.png') !!}</a>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <h2>Blog</h2>
                    </div>

                    <div class="col-md-6">
                        <h2>Blog Post #1</h2>
                    </div>

                    <div class="col-md-6">
                        <h2>Blog Post #2</h2>
                    </div>
                </div>
            </div>
            <!-- end class="col-md-8" -->

            <div id="leaderboard" class="col-md-4">
                <h2>Leaderboard</h2>

                <h3>All Time</h3>
                <hr>
                @foreach ($topusers_alltime as $user)
                    <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-2">
                            @if ($user->avatar == ''){!!  HTML::image('img/profile_pic.png', 'alt', array( 'width' => 60, 'height' => 60 )) !!}@else
                                <img src="{!! $user->avatar !!}" width="60" height="60"/> @endif
                        </div>
                        <div class="col-md-6 name">
                            <p>{{ $user->name }}</p>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                @endforeach
                <h3>Last month</h3>
                <hr>
                @foreach ($topusers_lastmonth as $user)
                    <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-2">
                            @if ($user->avatar == ''){!!  HTML::image('img/profile_pic.png', 'alt', array( 'width' => 60, 'height' => 60 )) !!}@else
                                <img src="{!! $user->avatar !!}" width="60" height="60"/> @endif
                        </div>
                        <div class="col-md-6">
                            <p>{{ $user->name }}</p>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
