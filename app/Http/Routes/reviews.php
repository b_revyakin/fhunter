<?php

Route::group(['middleware' => 'auth', 'as' => 'reviews.'], function () {

    Route::get('festival/{id}/write-review', ['as' => 'create', 'uses' => 'ReviewsController@create']);
    Route::post('festival/{id}/write-review', ['as' => 'store', 'uses' => 'ReviewsController@store']);

    Route::get('reviews', ['as' => 'index', 'uses' => 'ReviewsController@index']);
    Route::get('reviews/{id}', ['as' => 'show', 'uses' => 'ReviewsController@show']);
    Route::get('reviews/{id}/edit', ['as' => 'edit', 'uses' => 'ReviewsController@edit']);
    Route::post('reviews/{id}', ['as' => 'update', 'uses' => 'ReviewsController@update']);
    Route::post('reviews/{id}/delete', ['as' => 'destroy', 'uses' => 'ReviewsController@destroy']);
});
