@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ trans("admin/festivals.festivals") }} :: @parent
@stop

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Festival Routes
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{{ URL::route('admin.festival_routes.create') }}"
                       class="btn btn-sm  btn-primary iframe"><span
                                class="glyphicon glyphicon-plus-sign"></span>New</a>
                </div>
            </div>
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Count of festivals</th>
                <th>Start date</th>
                <th>Finish date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($festivalRoutes as $festivalRoute)
                <tr>
                    <td>{{ $festivalRoute->title }}</td>
                    <td>{{ $festivalRoute->description }}</td>
                    <td>{{ $festivalRoute->festivals->count() }}</td>
                    <td>{{ $festivalRoute->festivals->first()->start_date }}</td>
                    <td>{{ $festivalRoute->festivals->last()->start_date }}</td>
                    <td>
                        <a class="btn btn-info"
                           href="{{ route('festival_routes.show', [$festivalRoute->id]) }}">Show</a>
                        <a class="btn btn-info" href="{{ route('admin.festival_routes.edit', [$festivalRoute->id]) }}">Edit</a>

                        <form method="POST" action="{{ route('admin.festival_routes.delete', [$festivalRoute->id]) }}"
                              style="display: inline">
                            {{ csrf_field() }}
                            <button class="btn btn-info" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop
