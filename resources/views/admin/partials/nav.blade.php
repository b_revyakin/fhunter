<div class="input-group">
    <input type="text" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button">
            <i class="fa fa-search"></i>
        </button>
      </span>
</div>


<ul class="nav nav-pills nav-stacked" id="menu">
    <li {{ (Request::is('admin/dashboard') ? ' class=active' : '') }}>
        <a href="{{URL::to('admin/dashboard')}}"
                >
            <i class="fa fa-dashboard fa-fw"></i><span class="hidden-sm text">
Dashboard</span>
        </a>
    </li>
    <li {{ (Request::is('admin/festivals*') ? ' class=active' : '') }} >
        <a href="{{URL::to('admin/festivals')}}"
                >
            <i class="glyphicon glyphicon-bullhorn"></i><span
                    class="hidden-sm text"> Festivals</span>
        </a>
    </li>
    <li {{ (Request::is('admin/language*') ? ' class=active' : '') }}>
        <a href="{{URL::to('admin/language')}}"
                >
            <i class="fa fa-language"></i><span
                    class="hidden-sm text"> Language</span>
        </a>
    </li>
    <li {{ (Request::is('admin/news*') ? ' class=active' : '') }}>
        <a href="#">
            <i class="glyphicon glyphicon-bullhorn"></i> Blog items<span
                    class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level collapse">
            <li {{ (Request::is('admin/newscategory') ? ' class=active' : '') }} >
                <a href="{{URL::to('admin/newscategory')}}">
                    <i class="glyphicon glyphicon-list"></i><span
                            class="hidden-sm text"> Blog categories </span>
                </a>
            </li>
            <li {{ (Request::is('admin/news') ? ' class=active' : '') }} >
                <a href="{{URL::to('admin/news')}}">
                    <i class="glyphicon glyphicon-bullhorn"></i><span
                            class="hidden-sm text"> Blog</span>
                </a>
            </li>
        </ul>
    </li>
    <li {{ (Request::is('admin/photo*') ? ' class=active' : '') }}>
        <a href="#">
            <i class="glyphicon glyphicon-camera"></i> Photo items<span
                    class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level collapse" id="collapseTwo">
            <li {{ (Request::is('admin/photoalbum') ? ' class=active' : '') }} >
                <a href="{{URL::to('admin/photoalbum')}}">
                    <i class="glyphicon glyphicon-list"></i><span
                            class="hidden-sm text"> Photo albums</span>
                </a>
            </li>
            <li {{ (Request::is('admin/photo') ? ' class=active' : '') }}>
                <a href="{{URL::to('admin/photo')}}"
                        >
                    <i class="glyphicon glyphicon-camera"></i><span
                            class="hidden-sm text"> Photo</span>
                </a>
            </li>
        </ul>
    </li>

    <li {{ (Request::is('admin/users*') ? ' class=active' : '') }} >
        <a href="{{URL::to('admin/users')}}"
                >
            <i class="glyphicon glyphicon-user"></i><span
                    class="hidden-sm text"> Users</span>
        </a>
    </li>
    <li class="{{ (Request::routeLike(Request::fullUrl(), 'admin.festival_routes.index') ? 'active' : '') }}">
        <a href="{{ URL::route('admin.festival_routes.index') }}">
            <i class="glyphicon glyphicon-bullhorn"></i>
            <span class="hidden-sm text"> Festival Routes</span>
        </a>
    </li>
    <li class="{{ (Request::routeLike(Request::fullUrl(), 'admin.reviews.list') ? 'active' : '') }}">
        <a href="{{ URL::route('admin.reviews.list') }}">
            <i class="glyphicon glyphicon-bullhorn"></i>
            <span class="hidden-sm text"> Reviews</span>
        </a>
    </li>
</ul>
