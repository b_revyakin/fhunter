<?php

namespace App\Repositories\Rank;

use App\Entities\Rank;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class RankRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RankRepositoryEloquent extends BaseRepository implements RankRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Rank::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}