<?php

namespace App\Console\Commands;

use App\Repositories\User\UserRepository;
use Illuminate\Console\Command;

class ReCalculateUserPoints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user-points:recalculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate user\'s points for fix some errors';

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Create a new command instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->userRepository->all();
        foreach ($users as $user) {
            $user->points = 0;
            foreach ($user->reviews as $review) {
                $user->points += 5;//For rate

                if ($review->approved) {
                    $user->points += 50;
                }

                foreach ($review->tips as $tip) {
                    if ($tip->approved) {
                        $user->points += 1;
                    }
                }

                foreach ($review->photos as $photo) {
                    if ($photo->approved) {
                        $user->points += 10;
                    }
                }
            }
            $user->save();
        }
    }
}
