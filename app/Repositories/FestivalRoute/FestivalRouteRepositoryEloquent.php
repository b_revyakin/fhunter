<?php

namespace App\Repositories\FestivalRoute;

use App\Entities\FestivalRoute;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class FestivalRouteRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FestivalRouteRepositoryEloquent extends BaseRepository implements FestivalRouteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FestivalRoute::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}