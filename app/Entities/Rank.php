<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Rank extends Model implements Transformable
{
    use TransformableTrait;

    public $timestamps = false;
    protected $fillable = ['title', 'color', 'from', 'to'];

}
