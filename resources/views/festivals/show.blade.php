@extends('app')

@section('styles')
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/components/slick-carousel/slick/slick-theme.css">
    <link rel="stylesheet" href="/components/slick-carousel/slick/slick.css">
@stop

@section('scripts')
    <script src="/js/festival/show.js"></script>
    <script src="/components/slick-carousel/slick/slick.min.js"></script>
    <script>
        $(function () {
            $('#carousel').slick();
        });
    </script>
@stop

@section('content')
    <div class="container">
        <div class="row festival-subheader">
            <div class="col-md-4">
                <h4 class="header">{{ $festival->name }}</h4>
            </div>


            <div class="col-md-2 sm">
                <div class="header-button">
                    @if($plan['plan'])
                        <form method="POST"
                              action="{{ URL::route('festival.remove-from-plan', ['id' => $festival->id]) }}"
                              id="form-remove-from-plan">
                            {{ csrf_field() }}
                            <p>
                                <a id="remove-from-plan">Remove<br> from plan</a>
                            </p>
                        </form>
                    @else
                        <form method="POST" action="{{ URL::route('festival.add-to-plan', ['id' => $festival->id]) }}"
                              id="form-add-to-plan">
                            {{ csrf_field() }}
                            <p>
                                <a id="add-to-plan">Add<br> to plan</a>
                            </p>
                        </form>
                    @endif
                </div>
            </div>


            <div class="col-md-2 sm">
                <div class="header-button">
                    @if($plan['attender'])
                        <form method="POST"
                              action="{{ URL::route('festival.remove-been-here', ['id' => $festival->id]) }}"
                              id="form-remove-i-been-here">
                            {{ csrf_field() }}
                            <p>
                                <a id="remove-i-been-here">I not have<br> been here</a>
                            </p>
                        </form>
                    @else
                        <form method="POST"
                              action="{{ URL::route('festival.add-been-here', ['id' => $festival->id]) }}"
                              id="form-add-i-been-here">
                            {{ csrf_field() }}
                            <p>
                                <a id="add-i-been-here">I've<br> been here</a>
                            </p>
                        </form>
                    @endif
                </div>
            </div>

            <div class="col-md-2 sm">
                <div class="header-button">
                    @if($plan['want'])
                        <form method="POST"
                              action="{{ URL::route('festival.remove-from-want-list', ['id' => $festival->id]) }}"
                              id="form-remove-from-want-list">
                            {{ csrf_field() }}
                            <p>
                                <a id="remove-from-want-list">I not<br> want to go</a>
                            </p>
                        </form>
                    @else
                        <form method="POST"
                              action="{{ URL::route('festival.add-to-want-list', ['id' => $festival->id]) }}"
                              id="form-add-to-want-list">
                            {{ csrf_field() }}
                            <p>
                                <a id="add-to-want-list">I<br> want to go</a>
                            </p>
                        </form>
                    @endif
                </div>
            </div>

            <div class="col-md-2 sm">
                <div class="header-button">
                    <p><a href="{{ route('reviews.create', $festival->id) }}">Review/<br>Submit Photos</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="page-header height-400">
                <div class="row">
                    <div class="col-md-12 head-title">
                        {{ $festival->name }}
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div> <!-- end page-header -->

    <div class="container">
        <div class="row">
            <div class="col-md-12" data-slick='{"slidesToShow": 4, "slidesToScroll": 4}' id="carousel">
                <div class="img-carousel"><img src="/img/submit-photos.png" alt=""></div>
                <div class="img-carousel"><img src="/img/submit-photos.png" alt=""></div>
                <div class="img-carousel"><img src="/img/submit-photos.png" alt=""></div>
                <div class="img-carousel"><img src="/img/submit-photos.png" alt=""></div>
                <div class="img-carousel"><img src="/img/submit-photos.png" alt=""></div>
                <div class="img-carousel"><img src="/img/submit-photos.png" alt=""></div>
            </div>
            <div class="col-md-8">
                <article>
                    <div class="body">
                        <p><b>Headliner:</b> {{ $festival->headliners }}</p>

                        <p><b>Dates:</b> {{ $festival->start_date }}{{ $festival->end_date }}</p>

                        <p><b>Accomodation:</b> {{ $festival->accomodation }}</p>

                        <p><b>Why to go:</b> {{ $festival->why_to_go }}</p>

                        <p><b>Official website:</b> <a href="{{ $festival->website }}">{{ $festival->website }}</a></p>

                        <p><b>Top tips:</b></p>

                        <p><b>Other artists:</b> {{ $festival->showArtists() }} </p>

                        <p><b>Info:</b> {{ $festival->info }}</p>
                    </div>
                    <div class="col-md-2">
                    </div>
                </article>
            </div>
        </div>
    </div>
@stop
