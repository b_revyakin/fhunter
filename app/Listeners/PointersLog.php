<?php

namespace App\Listeners;

use App\Entities\Festival;
use App\Entities\Review;
use App\Entities\TopTip;
use App\Events\Event;
use App\Repositories\UserPointCause\UserPointCauseRepository;
use App\Repositories\UserPointLog\UserPointLogRepository;
use App\Traits\DbTransactionTrait;
use Exception;

class PointersLog
{
    use DbTransactionTrait;

    /**
     * @var UserPointLogRepository
     */
    private $log;

    /**
     * @var UserPointCauseRepository
     */
    private $cause;

    /**
     * Create the event listener.
     *
     * @param UserPointLogRepository $log
     * @param UserPointCauseRepository $cause
     */
    public function __construct(UserPointLogRepository $log, UserPointCauseRepository $cause)
    {
        $this->log = $log;
        $this->cause = $cause;
    }

    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $causeObject = $event->getCauseObject();
        $user = $event->getUser();
        $cause = $this->cause->findBySlug($event->getCause());

        $this->beginTransaction();

        try {
            $this->log->create([
                'user_id' => $user->id,
                'cause_id' => $cause->id,
                'cause_description' => $event->getCauseDescription(),
                'festival_id' => ($causeObject instanceof Festival ? $causeObject->id : null),
                'review_id' => ($causeObject instanceof Review ? $causeObject->id : null),
                'top_tip_id' => ($causeObject instanceof TopTip ? $causeObject->id : null)
            ]);

            //ToDo: need check what the event and how many time can increase points!
            $user->points += $cause->points;
            $user->save();

            $this->commit();
        } catch (Exception $e) {
            $this->rollBackAndLogException($e);
        }

    }
}
