@extends('app')

@section('scripts')
    <script src="/js/festival/review.js"></script>
@stop

@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 center-align user-mini-dash">
                    @include('includes.profile')
                </div>
                <div id="review" class="col-md-9">
                    {!! Form::open(array('class' => 'form-control', 'files' => true, 'method' => 'post', 'url' => route('reviews.update', $review->id))) !!}
                    <div class="row">
                        <div class="col-md-8 form-group">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <h2>Submit Review</h2>

                                    <p>You can submit one or all</p>
                                    {!! Form::select('festival_id', $festivals, $review->festival->id, array('width' => '400', 'style' => 'width: 100%;')) !!}
                                </div>
                                <div class="col-md-6 form-group">
                                    <h4>Add Photos</h4>
                                    {!! Form::file('photos[]', ['multiple']) !!}
                                </div>
                            </div>
                            <div class="background-green">
                                Stars:
                                <input type="hidden" name="stars" value="{{ $review->stars }}"/>
                                <i class='glyphicon glyphicon-star star' data-star="1"></i>
                                <i class='glyphicon glyphicon-star star' data-star="2"></i>
                                <i class='glyphicon glyphicon-star star' data-star="3"></i>
                                <i class='glyphicon glyphicon-star star' data-star="4"></i>
                                <i class='glyphicon glyphicon-star star' data-star="5"></i>
                            </div>
                            {!! Form::textarea('review', $review->review, ['rows' => 8, 'required']) !!}
                        </div>
                        <div class="col-md-4">
                            <h4>Submit Top Tips</h4>
                            @foreach($review->tips as $tip)
                                {!! Form::text('toptips[]', $tip->tip, ['maxlength' => '50']) !!}
                            @endforeach
                            @for($i = 0; $i < (5 - $review->tips->count()); $i++)
                                {!! Form::text('toptips[]', '', ['maxlength' => '50']) !!}
                            @endfor

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-8">
                            {!! Form::submit() !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

        </div>
    </div>
@endsection
@stop
