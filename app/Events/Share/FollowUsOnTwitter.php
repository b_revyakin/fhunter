<?php

namespace App\Events\Share;

use App\Entities\User;

class FollowUsOnTwitter extends LikeEvent
{
    /**
     * LikeEvent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);

        $this->causeDescription = 'Follow Us on Twitter';
    }

}
