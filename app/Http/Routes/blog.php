<?php

Route::group(['prefix' => 'blog'], function () {

    Route::get('', ['as' => 'news', 'uses' => 'BlogController@index']);
    Route::get('{id}', ['as' => 'news.view', 'uses' => 'BlogController@show']);

});
