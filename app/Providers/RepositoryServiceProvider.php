<?php

namespace App\Providers;

use App\Repositories\Article\ArticleRepository;
use App\Repositories\Article\ArticleRepositoryEloquent;
use App\Repositories\Festival\FestivalRepository;
use App\Repositories\Festival\FestivalRepositoryEloquent;
use App\Repositories\FestivalRoute\FestivalRouteRepository;
use App\Repositories\FestivalRoute\FestivalRouteRepositoryEloquent;
use App\Repositories\Rank\RankRepository;
use App\Repositories\Rank\RankRepositoryEloquent;
use App\Repositories\Review\ReviewRepository;
use App\Repositories\Review\ReviewRepositoryEloquent;
use App\Repositories\ReviewPhoto\ReviewPhotoRepository;
use App\Repositories\ReviewPhoto\ReviewPhotoRepositoryEloquent;
use App\Repositories\TopTip\TopTipRepository;
use App\Repositories\TopTip\TopTipRepositoryEloquent;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryEloquent;
use App\Repositories\UserPlan\UserPlanRepository;
use App\Repositories\UserPlan\UserPlanRepositoryEloquent;
use App\Repositories\UserPointCause\UserPointCauseRepository;
use App\Repositories\UserPointCause\UserPointCauseRepositoryEloquent;
use App\Repositories\UserPointLog\UserPointLogRepository;
use App\Repositories\UserPointLog\UserPointLogRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ArticleRepository::class,
            ArticleRepositoryEloquent::class
        );

        $this->app->bind(
            FestivalRepository::class,
            FestivalRepositoryEloquent::class
        );

        $this->app->bind(
            RankRepository::class,
            RankRepositoryEloquent::class
        );

        $this->app->bind(
            UserRepository::class,
            UserRepositoryEloquent::class
        );

        $this->app->bind(
            UserPlanRepository::class,
            UserPlanRepositoryEloquent::class
        );

        $this->app->bind(
            ReviewRepository::class,
            ReviewRepositoryEloquent::class
        );

        $this->app->bind(
            ReviewPhotoRepository::class,
            ReviewPhotoRepositoryEloquent::class
        );

        $this->app->bind(
            TopTipRepository::class,
            TopTipRepositoryEloquent::class
        );

        $this->app->bind(
            FestivalRouteRepository::class,
            FestivalRouteRepositoryEloquent::class
        );

        $this->app->bind(
            UserPointCauseRepository::class,
            UserPointCauseRepositoryEloquent::class
        );

        $this->app->bind(
            UserPointLogRepository::class,
            UserPointLogRepositoryEloquent::class
        );
    }
}
