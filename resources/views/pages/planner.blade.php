@extends('app')

@section('title') Home :: @parent @stop

@section('styles')
    <link rel="stylesheet" href="/css/common.css">
@stop

@section('scripts')
    <script src="/js/my-planner.js"></script>
@stop

@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 center-align user-mini-dash">
                    @include('includes.profile', ['planners' => $planners, 'attenders' => $attenders])
                </div>
                <div class="col-md-4">
                    <h2>Festivals I've been to</h2>
                    <ul class="festivals-list">
                        @foreach ($attenders as $festival)
                            <a href="{{ URL::route('festival', ['id' => $festival->id]) }}">
                                <li>{{ $festival->name }}</li>
                            </a>
                        @endforeach

                    </ul>
                </div>
                <div class="col-md-5">
                    <h2>Festivals I want to go</h2>
                    <ul class="festivals-list">
                        @foreach ($wants as $festival)
                            <a href="{{ URL::route('festival', ['id' => $festival->id]) }}">
                                <li>{{ $festival->name }}</li>
                            </a>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>My Planner</h1>

                <h2>This will be big calendar with planned festivals</h2>
                @foreach ($planners as $plan)
                    <a class="black" href="{{ URL::route('festival', ['id' => $festival->id]) }}">
                        <li>{{ $festival->name }}: {{ $festival->start_date }}</li>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@stop
