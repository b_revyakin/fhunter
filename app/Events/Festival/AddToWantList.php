<?php

namespace App\Events\Festival;

use App\Entities\Festival;
use App\Entities\User;

class AddToWantList extends FestivalEvent
{
    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Festival $festival
     */
    public function __construct(User $user, Festival $festival)
    {
        parent::__construct($user, $festival);

        $this->causeSlug = 'planner.want';
        $this->causeDescription = 'User add festival to his want list';
    }
}
