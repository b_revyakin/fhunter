@extends('admin.layouts.default')

@section('title')
    {{ 'Reviews' }} :: @parent
@stop

@section('main')
    <div class="page-header">
        <h3>
            Reviews
        </h3>
    </div>

    <div class="row">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Status</th>
                <th>Festival</th>
                <th>User</th>
                <th>Stars</th>
                <th>Text</th>
                <th>Count of photos</th>
                <th>Count of top tips</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reviews as $review)
                <tr>
                    <td>
                        @if($review->approved === null)
                            Wait
                        @else
                            {{ $review->approved ? 'Approved' : 'Rejected' }}
                        @endif
                    </td>
                    <td><a href="{{ route('festival', $review->festival->id) }}">{{ $review->festival->name }}</a></td>
                    <td>{{ $review->user->name }}</td>
                    <td>{{ $review->stars }}</td>
                    <td>{{ $review->review }}</td>
                    <td>{{ $review->photos()->approved()->get()->count() }} ({{ $review->photos()->get()->count() }})
                    </td>
                    <td>{{ $review->tips()->approved()->get()->count() }} ({{ $review->tips()->get()->count() }})</td>
                    <td><a class="btn btn-info btn-xs" href="{{ route('admin.reviews.show', $review->id) }}">Show</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@stop
