<?php

namespace App\Http\Requests;

class ReviewRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'festival_id' => 'required|exists:festivals,id',
            'photos' => 'array',
            'review' => 'required|string',
            'toptips' => 'array',
            'stars' => 'required|in:1,2,3,4,5'
        ];
    }
}
