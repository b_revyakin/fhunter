<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Review extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['festival_id', 'user_id', 'stars', 'review', 'approved'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function festival()
    {
        return $this->belongsTo(Festival::class);
    }

    public function photos()
    {
        return $this->hasMany(ReviewPhoto::class);
    }

    public function tips()
    {
        return $this->hasMany(TopTip::class);
    }
}
