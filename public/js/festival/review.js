$(function () {
  (function () {
    var rate = $('input[name=\'stars\']').val();

    changeRate(rate);
  })();


  $('.star').on('click', function(event, elem) {
    var rate = $(this).data('star');
    $('input[name=\'stars\']').val(rate);
    changeRate(rate);
  });

  function changeRate(rate) {
    var starArray = $('.star');
    starArray.slice(0, rate + 1).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
    starArray.slice(rate).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
  }


});
