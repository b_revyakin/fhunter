<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class FestivalRoute extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['title', 'description'];

    /**
     * Property for caching festivals.
     *
     * @var \Illuminate\Database\Eloquent\Collection|null
     */
    protected $festivals;

    public function attachFestival(Festival $festival, $additionData = null)
    {
        return (!$this->getFestivals()->contains($festival)) ? $this->festivals()->attach($festival, $additionData) : true;
    }

    public function getFestivals()
    {
        return (!$this->festivals) ? $this->festivals = $this->festivals()->get() : $this->festivals;
    }

    public function festivals()
    {
        return $this->belongsToMany(Festival::class, 'festival_route_pivot', 'route_id', 'festival_id')
            ->withPivot('position')
            ->orderBy('position');
    }

    public function detachFestival(Festival $festival)
    {
        $this->festivals = null;

        return $this->festivals()->detach($festival);
    }

}
