<?php

namespace App\Repositories\Rank;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RankRepository
 * @package namespace App\Repositories;
 */
interface RankRepository extends RepositoryInterface
{
    //
}
