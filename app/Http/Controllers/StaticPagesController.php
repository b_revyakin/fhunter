<?php

namespace App\Http\Controllers;

class StaticPagesController extends Controller
{

    public function getTerms()
    {
        return view('static-pages.terms');
    }

    public function getPrivacy()
    {
        return view('static-pages.privacy');
    }

    public function getContacts()
    {
        return view('static-pages.contacts');
    }

    public function getWeAre()
    {
        return view('static-pages.we-are');
    }

    public function getAbout()
    {
        return view('static-pages.about');
    }

    /**
     * Handle calls to missing methods on the controller.
     *
     * @param  array $parameters
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function missingMethod($parameters = array())
    {
        abort(404);
    }

}
