<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFestivalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('festivals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('lastfm_fest_id');
			$table->string('name');
			$table->string('city');
			$table->string('country');
			$table->string('street');
			$table->string('postalcode');
			$table->string('geo_lat');
			$table->string('geo_long');
			$table->json('headliners');
			$table->string('start_date');//ToDo: need change to date and do parser in FetchFestivals
			$table->string('end_date');
			$table->integer('size');
			$table->integer('cost');
			$table->string('info', 1024);
			$table->string('why_to_go');
			$table->string('website');
			$table->json('other_artists');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('festivals');
	}

}