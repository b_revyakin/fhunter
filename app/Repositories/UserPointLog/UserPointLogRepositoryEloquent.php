<?php

namespace App\Repositories\UserPointLog;

use App\Entities\UserPointLog;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserPointLogRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserPointLogRepositoryEloquent extends BaseRepository implements UserPointLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserPointLog::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}