<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Review\ReviewRepository;
use App\Services\ReviewService;
use Exception;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    private $review;

    private $reviewRepository;

    function __construct(ReviewRepository $reviewRepository, ReviewService $review)
    {
        $this->reviewRepository = $reviewRepository;
        $this->review = $review;
    }

    public function showReviews()
    {
        $reviews = $this->reviewRepository->all();
        return view('admin.review.list', compact('reviews'));
    }

    public function showReview($reviewId)
    {
        $review = $this->reviewRepository->find($reviewId);
        return view('admin.review.show', compact('review'));
    }

    public function submitReview(Request $request, $reviewId)
    {
        try {
            $this->review->submitReview($request->all(), $reviewId);
            //ToDo: add success flash message
            return redirect()->route('admin.reviews.list');
        } catch (Exception $e) {
            //ToDo: add error flash message
            return redirect()->back();
        }
    }
}
