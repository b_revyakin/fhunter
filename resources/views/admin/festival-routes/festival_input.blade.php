<div class="{{ ($selectedFestival === null ? 'hidden' : null) }}"
     id="{{ ($selectedFestival === null ? 'festival-select' : null) }}">
    <select name="festival[]">
        @foreach($festivals as $festival)
            <option {{ (($selectedFestival && $festival->id == $selectedFestival->id) ? 'selected' : null) }} value="{{ $festival->id }}">{{ $festival->name . ' | ' . $festival->start_date }}</option>
        @endforeach
    </select>
    <button type="button" class="btn btn-danger btn-remove-festival">Remove</button>
    <button type="button" class="btn btn-info btn-up-festival">Up</button>
    <button type="button" class="btn btn-info btn-down-festival">Down</button>
</div>
