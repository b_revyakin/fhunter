@extends('app')

@section('styles')
    <link rel="stylesheet" href="/css/common.css">
@stop

@section('scripts')
    <script src="/js/festival/show.js"></script>
@stop

@section('content')
    <div class="container">
        <div class="row festival-subheader">
            <div class="col-md-4">
                <h4>{{ $festivalRoute->title }}</h4>
            </div>
        </div>
    </div>

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h1>Festival Route</h1>
                </div>
                <div class="col-md-9">
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div> <!-- end page-header -->

    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                {!! $map['html'] !!}
            </div>
        </div>
    </div>


@endsection
@stop
