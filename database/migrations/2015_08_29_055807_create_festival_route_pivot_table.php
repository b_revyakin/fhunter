<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFestivalRoutePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festival_route_pivot', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('festival_id')->unsigned();
            $table->foreign('festival_id')->references('id')->on('festivals')->onDelete('cascade');

            $table->integer('route_id')->unsigned();
            $table->foreign('route_id')->references('id')->on('festival_routes')->onDelete('cascade');

            $table->integer('position')->unsigned();

            $table->unique(['route_id', 'festival_id']);
            $table->unique(['route_id', 'position']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('festival_route_pivot');
    }
}
