<?php

namespace App\Traits;

use App\Entities\Festival;

trait HasFestival
{
    /**
     * Property for caching festivals.
     *
     * @var \Illuminate\Database\Eloquent\Collection|null
     */
    protected $festivals;

    /**
     * Attach role to a user.
     *
     * @param int|Festival $festival
     * @return null|bool
     */
    public function attachFestival($festival, $type)
    {
        return (!$this->getFestivals($type)->contains($festival)) ? $this->festivals()->attach($festival, ['type' => $type]) : true;
    }

    /**
     * Get all roles as collection.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFestivals($type)
    {
        return (!$this->festivals) ? $this->festivals = $this->festivals()->wherePivot('type', $type)->get() : $this->festivals;
    }

    /**
     * Entity belongs to many festivals.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function festivals()
    {
        return $this->belongsToMany(Festival::class);
    }

    /**
     * Detach role from a user.
     *
     * @param int|Festival $festival
     * @return int
     */
    public function detachFestival($festival, $type)
    {
        $this->festivals = null;

        return $this->festivals()->wherePivot('type', $type)->detach($festival);
    }
}