<?php

namespace App\Repositories\ReviewPhoto;

use App\Entities\ReviewPhoto;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ReviewPhotoRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ReviewPhotoRepositoryEloquent extends BaseRepository implements ReviewPhotoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ReviewPhoto::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}