<?php

namespace App\Events\Review;

use App\Entities\Review;
use App\Entities\User;
use App\Events\Event;

abstract class ReviewEvent extends Event
{
    /**
     * ReviewEvent constructor.
     * @param Review $review
     * @param User $user
     */
    public function __construct(Review $review, User $user)
    {
        parent::__construct($user);

        $this->causeObject = $review;
    }
}
