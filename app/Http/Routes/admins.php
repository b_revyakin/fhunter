<?php

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    //ToDo: fix id2. It's not REST!


    //ToDo: use named routes!

    #Reviews
    Route::group(['prefix' => 'reviews', 'as' => 'reviews.'], function () {
        Route::get('', ['as' => 'list', 'uses' => 'ReviewsController@showReviews']);
        Route::get('{id}', ['as' => 'show', 'uses' => 'ReviewsController@showReview']);
        Route::post('{id}', ['as' => 'submit', 'uses' => 'ReviewsController@submitReview']);
    });

    #Festival Routes
    //For method show admin will open front sNhow festival route page
    /*Route::resource('festival_routes', 'FestivalRoutesController',
        ['except' => 'show']);*/
    Route::group(['prefix' => 'festival_routes', 'as' => 'festival_routes.'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'FestivalRoutesController@index']);
        Route::get('create', ['as' => 'create', 'uses' => 'FestivalRoutesController@create']);
        Route::post('store', ['as' => 'store', 'uses' => 'FestivalRoutesController@store']);
        Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'FestivalRoutesController@edit']);
        Route::post('{id}/update', ['as' => 'update', 'uses' => 'FestivalRoutesController@update']);
        Route::post('{id}/delete', ['as' => 'delete', 'uses' => 'FestivalRoutesController@destroy']);
    });


# Admin Dashboard
    Route::get('dashboard', 'DashboardController@index');

# Language
    Route::get('language', 'LanguageController@index');
    Route::get('language/create', 'LanguageController@getCreate');
    Route::post('language/create', 'LanguageController@postCreate');
    Route::get('language/{id}/edit', 'LanguageController@getEdit');
    Route::post('language/{id}/edit', 'LanguageController@postEdit');
    Route::get('language/{id}/delete', 'LanguageController@getDelete');
    Route::post('language/{id}/delete', 'LanguageController@postDelete');
    Route::get('language/data', 'LanguageController@data');
    Route::get('language/reorder', 'LanguageController@getReorder');

# News category
    Route::get('newscategory', 'ArticleCategoriesController@index');
    Route::get('newscategory/create', 'ArticleCategoriesController@getCreate');
    Route::post('newscategory/create', 'ArticleCategoriesController@postCreate');
    Route::get('newscategory/{id}/edit', 'ArticleCategoriesController@getEdit');
    Route::post('newscategory/{id}/edit', 'ArticleCategoriesController@postEdit');
    Route::get('newscategory/{id}/delete', 'ArticleCategoriesController@getDelete');
    Route::post('newscategory/{id}/delete', 'ArticleCategoriesController@postDelete');
    Route::get('newscategory/data', 'ArticleCategoriesController@data');
    Route::get('newscategory/reorder', 'ArticleCategoriesController@getReorder');

# News
    Route::get('news', 'BlogController@index');
    Route::get('news/create', 'BlogController@getCreate');
    Route::post('news/create', 'BlogController@postCreate');
    Route::get('news/{id}/edit', 'BlogController@getEdit');
    Route::post('news/{id}/edit', 'BlogController@postEdit');
    Route::get('news/{id}/delete', 'BlogController@getDelete');
    Route::post('news/{id}/delete', 'BlogController@postDelete');
    Route::get('news/data', 'BlogController@data');
    Route::get('news/reorder', 'BlogController@getReorder');

# Photo Album
    Route::get('photoalbum', 'PhotoAlbumController@index');
    Route::get('photoalbum/create', 'PhotoAlbumController@getCreate');
    Route::post('photoalbum/create', 'PhotoAlbumController@postCreate');
    Route::get('photoalbum/{id}/edit', 'PhotoAlbumController@getEdit');
    Route::post('photoalbum/{id}/edit', 'PhotoAlbumController@postEdit');
    Route::get('photoalbum/{id}/delete', 'PhotoAlbumController@getDelete');
    Route::post('photoalbum/{id}/delete', 'PhotoAlbumController@postDelete');
    Route::get('photoalbum/data', 'PhotoAlbumController@data');
    Route::get('photoalbum/reorder', 'PhotoAlbumController@getReorder');

# Photo
    Route::get('photo', 'PhotoController@index');
    Route::get('photo/create', 'PhotoController@getCreate');
    Route::post('photo/create', 'PhotoController@postCreate');
    Route::get('photo/{id}/edit', 'PhotoController@getEdit');
    Route::post('photo/{id}/edit', 'PhotoController@postEdit');
    Route::get('photo/{id}/delete', 'PhotoController@getDelete');
    Route::post('photo/{id}/delete', 'PhotoController@postDelete');
    Route::get('photo/{id}/itemsforalbum', 'PhotoController@itemsForAlbum');
    Route::get('photo/{id}/{id2}/slider', 'PhotoController@getSlider');
    Route::get('photo/{id}/{id2}/albumcover', 'PhotoController@getAlbumCover');
    Route::get('photo/data/{id}', 'PhotoController@data');
    Route::get('photo/reorder', 'PhotoController@getReorder');

# Video
    Route::get('videoalbum', 'VideoAlbumController@index');
    Route::get('videoalbum/create', 'VideoAlbumController@getCreate');
    Route::post('videoalbum/create', 'VideoAlbumController@postCreate');
    Route::get('videoalbum/{id}/edit', 'VideoAlbumController@getEdit');
    Route::post('videoalbum/{id}/edit', 'VideoAlbumController@postEdit');
    Route::get('videoalbum/{id}/delete', 'VideoAlbumController@getDelete');
    Route::post('videoalbum/{id}/delete', 'VideoAlbumController@postDelete');
    Route::get('videoalbum/data', 'VideoAlbumController@data');
    Route::get('video/reorder', 'VideoAlbumController@getReorder');

# Video
    Route::get('video', 'VideoController@index');
    Route::get('video/create', 'VideoController@getCreate');
    Route::post('video/create', 'VideoController@postCreate');
    Route::get('video/{id}/edit', 'VideoController@getEdit');
    Route::post('video/{id}/edit', 'VideoController@postEdit');
    Route::get('video/{id}/delete', 'VideoController@getDelete');
    Route::post('video/{id}/delete', 'VideoController@postDelete');
    Route::get('video/{id}/itemsforalbum', 'VideoController@itemsForAlbum');
    Route::get('video/{id}/{id2}/albumcover', 'VideoController@getAlbumCover');
    Route::get('video/data/{id}', 'VideoController@data');
    Route::get('video/reorder', 'VideoController@getReorder');

# Users
    Route::get('users/', 'UserController@index');
    Route::get('users/create', 'UserController@getCreate');
    Route::post('users/create', 'UserController@postCreate');
    Route::get('users/{id}/edit', 'UserController@getEdit');
    Route::post('users/{id}/edit', 'UserController@postEdit');
    Route::get('users/{id}/delete', 'UserController@getDelete');
    Route::post('users/{id}/delete', 'UserController@postDelete');
    Route::get('users/data', 'UserController@data');

# Festivals
    Route::get('festivals/', 'FestivalController@index');
    Route::get('festivals/create', 'FestivalController@getCreate');
    Route::post('festivals/create', 'FestivalController@postCreate');
    Route::get('festivals/{id}/edit', 'FestivalController@getEdit');
    Route::post('festivals/{id}/edit', 'FestivalController@postEdit');
    Route::get('festivals/{id}/delete', 'FestivalController@getDelete');
    Route::post('festivals/{id}/delete', 'FestivalController@postDelete');
    Route::get('festivals/data', 'FestivalController@data');

});
