<?php

use App\Entities\Article;
use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class ArticlesTableSeeder extends Seeder
{

	public function run()
	{
        TestDummy::times(4)->create(Article::class);
	}

}
