<?php

namespace App\Facades;

use URL;

class Request extends \Illuminate\Support\Facades\Request
{
    public static function routeLike($fullUrl, $routeName, $parameters = array())
    {
        return str_contains($fullUrl, URL::route($routeName, $parameters));
    }
}
